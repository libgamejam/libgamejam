#include "gamejam.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <time.h>
#include <termios.h>

int sw = 22;
int sh = 12;
int offX = 0;
int offY = 0;

void initsprites();

void drawchesspiece(int x, int y, int sid) {
	draw_sprite(offX + x*sw, offY + y*sh, sid);
}

int main(int argc, char** argv) {
	struct termios origtermsettings; // original terminal settings
	if (!draw_init(32, &origtermsettings))
		return 1;
	
	offX = (draw_term_get_cols()/2) - (sw * 8 / 2) ;
	offY = (draw_term_get_rows()/2) - (sh * 8 / 2) ;

	initsprites();


	//printf("test\n");

	int i;
	/*int j;
	int k;*/
	/*while(1) {
		k = 0;
		for (j = 1; j < 9;j++) {
			k = j;
			for (i = 1; i < 9;i++) {
				if (k % 2 != 0)
					draw_sprite(i*20,j*10, 0);
				else
					draw_sprite(i*20,j*10, 1);
				k++;
			}
		}
		draw_color_bg(COLOR_RED);
		draw_rect_outline(19,9,20 * 8 + 1, 10 * 8 + 1, NULL);
		fflush( stdout );
		sleep(1);
		k = 0;
		for (j = 1; j < 9;j++) {
			k = j;
			for (i = 1; i < 9;i++) {
				if (k % 2 == 0)
					draw_sprite(i*20,j*10, 0);
				else
					draw_sprite(i*20,j*10, 1);
				k++;
			}
		}
		draw_color_bg(COLOR_RED);
		draw_rect_outline(19,9,20 * 8 + 1, 10 * 8 + 1, NULL);
		fflush( stdout );
		sleep(1);
	}*/
	draw_term_clear();
	draw_color_reset();
	/*k = 0;
	for (j = 0; j < 8;j++) {
		k = j;
		for (i = 0; i < 8;i++) {
			if (k % 2 != 0)
				draw_sprite(offX + i*sw, offY + j*sh, 0);
			else
				draw_sprite(offX + i*sw, offY + j*sh, 1);
			k++;
		}
	}*/
	/*draw_color_bg(COLOR_BRIGHT_WHITE);
	draw_color_fg(COLOR_BLACK);
	draw_rect_outline(offX-1,offY-1,sw * 8 + 1, sh * 8 + 1, NULL);*/
	fflush( stdout );
	//sleep(1);



	
/*                    
                                                       .::.                                              
                                            _()_       _::_                        
                                  _O      _/____\_   _/____\_                         
           _  _  _     ^^__      / //\    \      /   \      /                    
          | || || |   /  - \_   {     }    \____/     \____/                    
          |_______| <|    __<    \___/     (____)     (____)                    
    _     \__ ___ / <|    \      (___)      |  |       |  |                    
   (_)     |___|_|  <|     \      |_|       |__|       |__|                    
  (___)    |_|___|  <|______\    /   \     /    \     /    \                    
  _|_|_    |___|_|   _|____|_   (_____)   (______)   (______)                    
 (_____)  (_______) (________) (_______) (________) (________)                    
 /_____\  /_______\ /________\ /_______\ /________\ /________\                    
    2        3           4         5         6           7
 */                    
	
	/*drawchesspiece(0, 0, 3);
	drawchesspiece(7, 0, 3);
	drawchesspiece(1, 0, 4);
	drawchesspiece(6, 0, 4);
	drawchesspiece(2, 0, 5);
	drawchesspiece(5, 0, 5);
	drawchesspiece(3, 0, 6);
	drawchesspiece(4, 0, 7);
	for (i = 0; i < 8; i++)
		drawchesspiece(i, 1, 2);
	drawchesspiece(0, 7, 13);
	drawchesspiece(7, 7, 13);
	drawchesspiece(1, 7, 14);
	drawchesspiece(6, 7, 14);
	drawchesspiece(2, 7, 15);
	drawchesspiece(5, 7, 15);
	drawchesspiece(4, 7, 16);
	drawchesspiece(3, 7, 17);
	for (i = 0; i < 8; i++)
		drawchesspiece(i, 6, 12);
	fflush( stdout );*/
	int k = 0;
	i = 0;
	//const char * txt = "Hello World, I am Jake and this is my beautiful text. It is actually a really long text and thus it might even be larger than your window. But hey, that shouldn't be a problem as this is a really easy to work with tool. Yeaaah! I love it. Well I have to continue programming now. Good bye. Oh noooo! My text is not loooong enough! I neeed too cooontinuueeee until I reach the END of the Terminal. Why is my font so small? I could just enlarge the font, but writing this text will surely allow me to find other bugs too. Yes. That is my plan! Well now I am going to test this and if this works I am going to be really happy. If it doesn't work I am going to be really sad.";
	const char * txt = "Hello World, I am Jake and this is my beautiful text.";
	size_t len = strlen(txt);
	int x = 1;
	int y = 5;
	int width = draw_term_get_cols() -1;
	int height = 10;
	while (1) {
		draw_color_bg(COLOR_RED);
		draw_color_fg(COLOR_RED);
		draw_rect_fill(1,1,draw_term_get_cols() , draw_term_get_rows(), '#');
		draw_color_bg(COLOR_YELLOW);
		draw_color_fg(COLOR_MAGENTA);
 	 	draw_rect_outline(1,1,draw_term_get_cols() , draw_term_get_rows(), NULL); // creates a border around the terminal
		draw_color_bg(COLOR_BLUE);
		draw_color_fg(COLOR_WHITE);
		draw_rect_fill(x,y,width,height, ' ');
		draw_rect_outline(x,y,width,height, NULL);
		draw_color_bg(COLOR_BRIGHT_BLACK);
		draw_color_fg(COLOR_BRIGHT_BLACK);
		draw_rect_shadow(x,y,width,height, ' ');
		draw_color_bg(COLOR_YELLOW);
		draw_color_fg(COLOR_CYAN);
		draw_print_center(x+1,y+1,width-2, "Center:", -1);
		draw_print_center(x+1,y+2,width-2, txt, i);
		draw_print_center(x+1,y+4,width-2, "Tickertext:", -1);
		draw_print_tickertext(x+1,y+5,width-2,k++,txt, 5);
		i++;
		if (i > len)
			i = 0;
		fflush( stdout );
		//sleep(1);
		usleep(100000);
	}
























	/*
	draw_term_clear();
	
	printf("Hello World\n");
	int i, j, n;


  

  for (i = 0; i < 11; i++) {

    for (j = 0; j < 10; j++) {

      n = 10*i + j;

      if (n > 108) break;

      printf("\033[%dm %3d\033[m", n, n);

    }

    printf("\n");

  }

	for (i = 0; i < 16; i++) {
		draw_color_bg(i);
		printf(" %3d | ", i);
		for (j = 0; j < 16; j++) {
			draw_color_fg(j);
			printf(" %3d", j);
		}
		printf("\n");
		//sleep(1);
		draw_color_reset();
	}

	//sleep(1);
	draw_term_clear();

	draw_color_bg(6);
	draw_rect_fill(10,20,10,20,'P');
	fflush( stdout );
	usleep(1000);
	draw_color_bg(5);
	draw_rect_outline(10,20,10,20,NULL);

	for (i = 0; i < 1059; i++) {
		draw_term_clear();
		draw_cursor_move(6,i);
		printf("yeet. rows: %3d cols: %3d\n", draw_term_get_rows(), draw_term_get_cols());
		draw_rect_outline(0,0,draw_term_get_cols(),draw_term_get_rows(),NULL);
		fflush(stdout);
		//usleep(1000);
	}
	printf("Bye World\n");
*/
	draw_destroy(&origtermsettings);

	return 0;
}

void initsprites() {
	//printf("DEBUG: initsprites!\n");
	draw_sprite_set(0, 22, 12,"\
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      ","\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888","\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888\
8888888888888888888888", '.');
	draw_sprite_set(1, 22, 12,"\
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      \
                      ","\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777","\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777\
7777777777777777777777", '.');
	draw_sprite_set(2, 22, 12,"\
......................\
......................\
......................\
......................\
......................\
......................\
..........._..........\
..........(_).........\
.........(___)........\
........._|_|_........\
........(_____).......\
......../_____\\.......","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000", '.');
	draw_sprite_set(3, 22, 12,"\
......................\
......................\
......................\
........_.._.._.......\
.......| || || |......\
.......|_______|......\
.......\\__ ___ /......\
........|___|_|.......\
........|_|___|.......\
........|___|_|.......\
.......(_______)......\
......./_______\\......","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000", '.');
	draw_sprite_set(4, 22, 12,"\
......................\
......................\
......................\
.........^^__.........\
......../  - \\_.......\
......<|    __<.......\
......<|    \\.........\
......<|     \\........\
......<|______\\.......\
......._|____|_.......\
......(________)......\
....../________\\......","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000", '.');
	draw_sprite_set(5, 22, 12,"\
......................\
......................\
........._O...........\
......../ //\\.........\
.......{     }........\
........\\___/.........\
........(___).........\
.........|_|..........\
......../   \\.........\
.......(_____)........\
......(_______).......\
....../_______\\.......","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000", '.');
	draw_sprite_set(6, 22, 12,"\
......................\
........_()_..........\
......_/____\\_........\
......\\      /........\
.......\\____/.........\
.......(____).........\
........|  |..........\
........|__|..........\
......./    \\.........\
......(______)........\
.....(________).......\
...../________\\.......","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000", '.');
	draw_sprite_set(7, 22, 12,"\
,,,,,,,,.::.,,,,,,,,,,\
,,,,,,,,_::_,,,,,,,,,,\
,,,,,,_/____\\_,,,,,,,,\
,,,,,,\\      /,,,,,,,,\
,,,,,,,\\____/,,,,,,,,,\
,,,,,,,(____),,,,,,,,,\
,,,,,,,,|  |,,,,,,,,,,\
,,,,,,,,|__|,,,,,,,,,,\
,,,,,,,/    \\,,,,,,,,,\
,,,,,,(______),,,,,,,,\
,,,,,(________),,,,,,,\
,,,,,/________\\,,,,,,,","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000", ',');
	draw_sprite_set(12, 22, 12,"\
......................\
......................\
......................\
......................\
......................\
......................\
..........._..........\
..........(_).........\
.........(___)........\
........._|_|_........\
........(_____).......\
......../_____\\.......","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF", '.');
	draw_sprite_set(13, 22, 12,"\
......................\
......................\
......................\
........_.._.._.......\
.......| || || |......\
.......|_______|......\
.......\\__ ___ /......\
........|___|_|.......\
........|_|___|.......\
........|___|_|.......\
.......(_______)......\
......./_______\\......","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF", '.');
	draw_sprite_set(14, 22, 12,"\
......................\
......................\
......................\
.........^^__.........\
......../  - \\_.......\
......<|    __<.......\
......<|    \\.........\
......<|     \\........\
......<|______\\.......\
......._|____|_.......\
......(________)......\
....../________\\......","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF", '.');
	draw_sprite_set(15, 22, 12,"\
......................\
......................\
........._O...........\
......../ //\\.........\
.......{     }........\
........\\___/.........\
........(___).........\
.........|_|..........\
......../   \\.........\
.......(_____)........\
......(_______).......\
....../_______\\.......","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF", '.');
	draw_sprite_set(16, 22, 12,"\
......................\
........_()_..........\
......_/____\\_........\
......\\      /........\
.......\\____/.........\
.......(____).........\
........|  |..........\
........|__|..........\
......./    \\.........\
......(______)........\
.....(________).......\
...../________\\.......","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF", '.');
	draw_sprite_set(17, 22, 12,"\
,,,,,,,,.::.,,,,,,,,,,\
,,,,,,,,_::_,,,,,,,,,,\
,,,,,,_/____\\_,,,,,,,,\
,,,,,,\\      /,,,,,,,,\
,,,,,,,\\____/,,,,,,,,,\
,,,,,,,(____),,,,,,,,,\
,,,,,,,,|  |,,,,,,,,,,\
,,,,,,,,|__|,,,,,,,,,,\
,,,,,,,/    \\,,,,,,,,,\
,,,,,,(______),,,,,,,,\
,,,,,(________),,,,,,,\
,,,,,/________\\,,,,,,,","\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000\
0000000000000000000000","\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFFFFFFFF", ',');

}

