#include "gamejam.h"

#include <stdio.h>
#include <ctype.h>
#include <unistd.h>
#include <fcntl.h>
#include <termios.h>

/**
 * @brief Functions for reading input from stdin.
 *
 */

/**
 * @brief Initializes the input environment.
 *
 * @note The input environment only works properly when inside a draw environment!
 * @note You need to call input_destroy() after input_init() before exiting the program!
 * @note This will turn all reads on file descriptor STDIN_FILENO to non-blocking mode
 */
void input_init() {
	/*ICANON normally takes care that one line at a time will be processed
	that means it will return if it sees a "\n" or an EOF or an EOL*/
	/*struct termios t;
	tcgetattr(STDIN_FILENO, &t);
	t.c_lflag &= ~(ICANON); // unset
	tcsetattr(STDIN_FILENO, TCSANOW, &t);*/
	

	// turn all reads on file descriptor STDIN_FILENO to non-blocking mode
	fcntl (STDIN_FILENO, F_SETFL, O_NONBLOCK);
}

/**
 * @brief Destroys the input environment.
 */
void input_destroy() {
	/*struct termios t;
	tcgetattr(STDIN_FILENO, &t);
	t.c_lflag |= ICANON; // set
	tcsetattr(STDIN_FILENO, TCSANOW, &t);*/
	// TODO find a way to disable O_NONBLOCK
}


/**
 * @brief Reads input from stdin.
 *
 * @note This function only works properly in an input environment.
 * @note This function is non-blocking by default. If no char was read, state will be set to 0 and '\0' returned.
 * @note errno might be set (see read(2))
 *
 * @param[in]     flags bit flags for changing behaviour of function (see their individual documentation in gamejam.h)
 * @param[in,out] state (optional)if a char was read value will be set to 1, otherwise it will be set to 0
 *
 * @return the read character
 *
 * Examples:
 * Example 1: non-blocking read without changing anything
 * @code
 * 	int state, c;
 *      
 * 	while (1) {
 *	 	c = input_read(0, &state); // try reading one char into c
 *	 	if (state != 0)
 * 			printf("Read char: '%c'\n", c);
 * 		else
 * 			printf("No char read.\n");
 * 	}
 * @endcode
 *
 * Example 2: blocking read, converting arrow keys to WASD and casting to lower case
 * @code
 * 	int c;
 * 	while (1) {
 *	 	c = input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER | INPUT_FLAG_BLOCK, NULL); // read one char into c
 * 		printf("Read char: '%c'\n", c);
 * 	}
 * @endcode
 *
 */
int input_read(unsigned int flags, int *state) {
	if (state)
		*state = 0;
	
	int c;
	int ctmp = '\0';
	ssize_t s;

	int blocking = (flags & INPUT_FLAG_BLOCK); // if INPUT_FLAG_BLOCK is set, wait until some input is read
	do {
		if ((s = read(STDIN_FILENO, &c, 1)) != -1) // try reading one char into c
			blocking = 0; // input read
	} while (blocking);

	if (s == -1)
		return '\0'; // no input read

	if (state)
		*state = 1; // at least one char read

	// code for detecting and converting arrow keys
	if ((flags & INPUT_FLAG_ARROWSTOWASD) && ((char)c) == '\033') { // if the first value is esc
		s = read(STDIN_FILENO, &ctmp, 1); // try reading one char into ctmp
		if (s != -1 && ((char)ctmp) == '[') { // test if special char sequence
			s = read(STDIN_FILENO, &c, 1); // try reading one char into c
			if (s != -1) {
				switch((char)c) {
				case 'A':
					// code for arrow up
					c = 'w';
					break;
				case 'B':
					// code for arrow down
					c = 's';
					break;
				case 'C':
					// code for arrow right
					c = 'd';
					break;
				case 'D':
					// code for arrow left
					c = 'a';
					break;
				}
			}
		}
	}

	return (flags & INPUT_FLAG_TOLOWER) ? tolower((char)c) : (char)c;
}

