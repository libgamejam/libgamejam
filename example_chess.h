#ifndef GAMEJAM_CHESS
#define GAMEJAM_CHESS


#define CHESS_SPRITE_COUNT 32
#define CHESS_SPRITE_WIDTH 16
#define CHESS_SPRITE_HEIGHT 8

#define CHESS_TERM_MIN_COLS (CHESS_SPRITE_WIDTH * 8 + 2)
#define CHESS_TERM_MIN_ROWS (CHESS_SPRITE_HEIGHT * 8 + 2)

#define CHESS_SPRITE_ID_SQUARE_BLACK 0
#define CHESS_SPRITE_ID_SQUARE_WHITE 1
#define CHESS_SPRITE_ID_CURSOR_BLACK 2
#define CHESS_SPRITE_ID_CURSOR_WHITE 3
#define CHESS_SPRITE_ID_SELECTED_SOURCE 4
#define CHESS_SPRITE_ID_SELECTED_TARGET 5
#define CHESS_SPRITE_ID_SELECTED_TARGET_ENEMY 6

#define CHESS_SPRITE_OFFSET_WHITE 10
#define CHESS_SPRITE_OFFSET_BLACK 20

#define CHESS_MOVE_DIRECTION_WHITE (-1)
#define CHESS_MOVE_DIRECTION_BLACK (1)

#define chess_draw_sprite(x,y,sid) draw_sprite( offset_x_g + (x)* CHESS_SPRITE_WIDTH , offset_y_g + (y)* CHESS_SPRITE_HEIGHT, (sid))

/*                    
+----------------+ +----------------+ +----------------+ +----------------+ +----------------+ +----------------+
|                | |    _  _  _     | |      ^^__      | |      _O        | |      _()_      | |      _TT_      |
|                | |   | || || |    | |     /  - \_    | |     / //\      | |    _/____\_    | |    _/____\_    |
|       _        | |   |_______|    | |   <|    __<    | |     \___/      | |    \      /    | |    \      /    |
|      (_)       | |   \__ ___ /    | |   <|     \     | |     (___)      | |     \____/     | |     \____/     |
|     (___)      | |    |_|___|     | |   <|______\    | |     /   \      | |     /    \     | |     /    \     |
|     _|_|_      | |    |___|_|     | |    _|____|_    | |    (_____)     | |    (______)    | |    (______)    |
|    (_____)     | |   (_______)    | |   (________)   | |   (_______)    | |   (________)   | |   (________)   |
|    /__P__\     | |   /___R___\    | |   /___N____\   | |   /___B___\    | |   /___Q____\   | |   /___K____\   |
+----------------+ +----------------+ +----------------+ +----------------+ +----------------+ +----------------+
     0: Pawn            1: Rook            2: Knight          3: Bishop          4: Queen           5: King      
     __Based on Ascii-Art by Alefith 22.02.95__ <https://www.asciiart.eu/sports-and-outdoors/chess>

+----------------+ +----------------+ +----------------+ +----------------+ +----------------+ +----------------+
|................| |...._.._.._.....| |......^^__......| |......_O........| |......_()_......| |......_TT_......|
|................| |...| || || |....| |...../  - \_....| |...../ //\......| |...._/____\_....| |...._/____\_....|
|......._........| |...|_______|....| |...<|    __<....| |.....\___/......| |....\      /....| |....\      /....|
|......(_).......| |...\__ ___ /....| |...<|     \.....| |.....(___)......| |.....\____/.....| |.....\____/.....|
|.....(___)......| |....|_|___|.....| |...<|______\....| |...../   \......| |...../    \.....| |...../    \.....|
|....._|_|_......| |....|___|_|.....| |...._|____|_....| |....(_____).....| |....(______)....| |....(______)....|
|....(_____).....| |...(_______)....| |...(________)...| |...(_______)....| |...(________)...| |...(________)...|
|..../__P__\.....| |.../___R___\....| |.../___N____\...| |.../___B___\....| |.../___Q____\...| |.../___K____\...|
+----------------+ +----------------+ +----------------+ +----------------+ +----------------+ +----------------+
     0: Pawn            1: Rook            2: Knight          3: Bishop          4: Queen           5: King      
 */                    
enum chess_piece_type {
	CHESS_PAWN,   // Bauer
	CHESS_ROOK,   // Turm
	CHESS_KNIGHT, // Springer
	CHESS_BISHOP, // Läufer
	CHESS_QUEEN,  // Königen
	CHESS_KING    // König
};

enum chess_selection_type {
	CHESS_SELECTION_NONE,
	CHESS_SELECTION_SOURCE,
	CHESS_SELECTION_TARGET_NORMAL,
	CHESS_SELECTION_TARGET_ENEMY,
	CHESS_SELECTION_TARGET_ENPASSANT,
	CHESS_SELECTION_TARGET_CASTLING_QUEENSIDE,
	CHESS_SELECTION_TARGET_CASTLING_KINGSIDE
};

enum chess_player_type {
	CHESS_PLAYER_WHITE,
	CHESS_PLAYER_BLACK
};

enum chess_gamestate_type {
	CHESS_GAMESTATE_RUNNING,
	CHESS_GAMESTATE_END_CHECKMATE_BLACK, // white won
	CHESS_GAMESTATE_END_CHECKMATE_WHITE, // black won
	CHESS_GAMESTATE_END_RESIGNATION_WHITE, // black won
	CHESS_GAMESTATE_END_RESIGNATION_BLACK, // white won
	//CHESS_GAMESTATE_END_FORFEIT,
	CHESS_GAMESTATE_END_DRAW_BY_AGREEMENT,
	//CHESS_GAMESTATE_END_DRAW_THREEFOLD_REPITION,
	//CHESS_GAMESTATE_END_DRAW_FIFTY_MOVE_RULE,
	//CHESS_GAMESTATE_END_DRAW_DEAD_POSITION,
	CHESS_GAMESTATE_END_DRAW_STALEMATE
};

struct chess_piece {
	int sid; // sprite id
	enum chess_piece_type type;
	enum chess_player_type player;
	int lastmoved; // id of the move in which this piece got moved last // -1: hasn't moved yet
	int isalive;
	int x;
	int y;
};

struct chess_square {
	struct chess_piece * piece; // only set if it has a piece, NULL otherwise
	enum chess_selection_type seltype; // selection
	int x;
	int y;
	int iseven; // used for selecting tile color and generating the checkers patttern
};

struct chess_game {
	struct chess_piece whitepieces[15];
	struct chess_piece blackpieces[15];
	struct chess_piece whiteking;
	struct chess_piece blackking;
	struct chess_square board[64]; // [x*8 + y]
	struct chess_square * hand;
	int moveid; // current move id
	int cursor_x;
	int cursor_y;
	enum chess_player_type currentplayer;
	int waslastmovepawntwosquare; // was the last move a two square move performed by a pawn (important for en passant)
	int iswhiteincheck; // only updated AFTER each move
	int isblackincheck;
	int nummovesleftwhite;
	int nummovesleftblack;
};

struct chess_piece *  chess_get_piece_by_id(struct chess_game * game, int id);
struct chess_square * chess_get_square     (struct chess_game * game, int x, int y);
int                   chess_nummovesleft   (struct chess_game * game, enum chess_player_type player);
int                   chess_is_check       (struct chess_game * game, enum chess_player_type player);


int  chess_promotion          (struct chess_game * game, struct chess_square * square);
void chess_draw_promotion_menu(struct chess_square * square, int selected);

void chess_start_menu();
void chess_quit_menu(enum chess_gamestate_type * gamestate);

void chess_draw_cursor(struct chess_game * game);
void chess_draw_square(struct chess_square * cur);
void chess_redraw_all (struct chess_game * game);


void                 chess_copy_game_piece     (struct chess_piece * dst, struct chess_piece * src);
struct chess_piece * chess_copy_game_find_piece(struct chess_game * game, int x, int y);
void                 chess_copy_game_square    (struct chess_square * dst, struct chess_square * src, struct chess_game * dstgame);
void                 chess_copy_game           (struct chess_game * dst, struct chess_game * src);

int  chess_init_sprites();
void chess_init_piece  (struct chess_game * game, struct chess_piece * cur, enum chess_piece_type type, enum chess_player_type player, int x, int y);
void chess_init_game   (struct chess_game * game);


void chess_unselect_all     (struct chess_game * game);
int chess_select_legit_moves(struct chess_game * game, struct chess_piece * piece, int skipchecktest);
int chess_try_select_move   (struct chess_game * game, struct chess_piece * piece, struct chess_square * square, int * state, int skipchecktest, enum chess_selection_type emptysquareseltype);
int chess_move              (struct chess_game * game, struct chess_piece * piece, struct chess_square * dest);


#endif
