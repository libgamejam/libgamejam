#include "sprite_editor.h"
#include "gamejam.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>


struct se_data_s se_data;
int offset_x_g, offset_y_g;

void printUsage(char * program) {
	printf("Usage: %s <file> <new|open|export> [...]\n", program);
	printf("  %s <file> new <width> <height>  -- creates a new sprite\n", program);
	printf("  %s <file> open                  -- opens an existing sprite\n", program);
	printf("  %s <file> export                -- just prints out the libgamejam\n", program);
	printf("                                                  c-code for that sprite\n");
	printf("  Note: <width> and <height> need to be larger than 0.\n");
	printf("  Note: <file> is a path to a .sprite file.\n");
}


int main(int argc, char** argv) {

	int width;
	int height;
	

	if (argc == 5 && strcmp("new", argv[2]) == 0) {
		se_data.programaction = SE_ACTION_NEW;
	} else if (argc == 3 && strcmp("open", argv[2]) == 0) {
		se_data.programaction = SE_ACTION_OPEN;
	} else if (argc == 3 && strcmp("export", argv[2]) == 0) {
		se_data.programaction = SE_ACTION_EXPORT;
	} else {
		printUsage(argv[0]);
		return 1;
	}
	
	se_data.spritefile = argv[1];
	if (strlen(se_data.spritefile) > 512) {
		fprintf(stderr, "ERROR: <file> is too long. Max 512 chars allowed!\n");
		printUsage(argv[0]);
		return 1;
	}


	struct termios origtermsettings; // original terminal settings

	if (!draw_init(16, &origtermsettings)) { // init draw environment
		fflush(stderr);
		fprintf(stderr, "Couldn't init draw environment!\n");
		fflush(stderr);
		return 1;
	}
	if (se_data.programaction == SE_ACTION_NEW) {
		if (access(se_data.spritefile, F_OK) == 0) {
			draw_destroy(&origtermsettings); // destory draw environment
			fprintf(stderr, "ERROR: File already exists. Refusing to use 'new' on an already existing file!\nYou may want to try using 'open' instead.\n");
			return 1;
		}
		width = atoi(argv[3]);
		height = atoi(argv[4]);
		if (width <= 0 || height <= 0) {
			draw_destroy(&origtermsettings); // destory draw environment
			printUsage(argv[0]);
			return 1;
		}
		if (!se_init(width, height)) {
			//draw_destroy(&origtermsettings); // destory draw environment
			fflush(stdout);
			perror("Couldn't init sprite_editor");
			fflush(stderr);
			se_free();
			return 1;
		}
	} else {
		if (!se_load(se_data.spritefile)) {
			draw_destroy(&origtermsettings); // destory draw environment
			fflush(stdout);
			perror("Couldn't init sprite_editor or failed to load file");
			fflush(stderr);
			return 1;
		}
		width = se_data.width;
		height = se_data.height;
		se_data.issaved = 1;
		if (se_data.programaction == SE_ACTION_EXPORT) {
			draw_destroy(&origtermsettings); // destory draw environment
			draw_color_reset();
			se_print_sprite_as_c();
			se_free();
			return 0;
		}
	}

	int mincols, minrows;
	
	mincols = 174;
	minrows = 30;
	if (mincols < width * 2 + 16)
		mincols = width * 2 + 16;
	if (minrows < height * 3 + 13)
		minrows = height * 3 + 13;

	int cols, rows;
	cols = draw_term_get_cols();
	rows = draw_term_get_rows();
	if (cols < mincols || rows < minrows) {
		se_free();
		draw_term_clear();
		draw_destroy(&origtermsettings); // destory draw environment
		fprintf(stderr, "Your terminal is too small.\n");
		fprintf(stderr, "Current size: %dx%d\n", cols,rows);
		fprintf(stderr, "Minimum needed: %dx%d\n", mincols, minrows);
		return 1;
	}


	input_init();
	se_redraw_all();

	char c;
	int s;
	int running = 1;
	while (running) {
		/*
		 * F1:
		 * Read char:  27 c: '
		 * ead char:  79 c: 'O'
		 * Read char:  80 c: 'P'
		 *
		 * F2:
		 * Read char:  27 c: '
		 * ead char:  79 c: 'O'
		 * Read char:  81 c: 'Q'
		 *
		 * F3:
		 * Read char:  27 c: '
		 * ead char:  79 c: 'O'
		 * Read char:  82 c: 'R'
		 *
		 * F4:
		 * Read char:  27 c: '
		 * ead char:  79 c: 'O'
		 * Read char:  83 c: 'S'
		 *
		 * F5:
		 * Read char:  27 c: '
		 * ead char:  91 c: '['
		 * Read char:  49 c: '1'
		 * Read char:  53 c: '5'
		 * Read char: 126 c: '~'
		 *
		 * F6:
		 * Read char:  27 c: '
		 * ead char:  91 c: '['
		 * Read char:  49 c: '1'
		 * Read char:  55 c: '7'
		 * Read char: 126 c: '~'
		 *
		 * F7:
		 * Read char:  27 c: '
		 * ead char:  91 c: '['
		 * Read char:  49 c: '1'
		 * Read char:  56 c: '8'
		 * Read char: 126 c: '~'
		 *
		 * F8:
		 * Read char:  27 c: '
		 * ead char:  91 c: '['
		 * Read char:  49 c: '1'
		 * Read char:  57 c: '9'
		 * Read char: 126 c: '~'
		 *
		 * F9:
		 * Read char:  27 c: '
		 * ead char:  91 c: '['
		 * Read char:  50 c: '2'
		 * Read char:  48 c: '0'
		 * Read char: 126 c: '~'
		 */

		c = input_read(INPUT_FLAG_BLOCK, NULL); // read input
		if (c == '\033') { // escape (might indicate a special character)
			c = input_read(0, &s);
			if (s != -1 && c == '[') { // test if special char sequence
				c = input_read(0, NULL); // read input
				if (c == '1') {
					c = input_read(0, NULL); // read input
					if (c == ';') {
						c = input_read(0, NULL); // read input
						if (c == '2') { // shift special char
							c = input_read(0, NULL); // read input
							switch(c) {
							case 'A': se_data.cury-=5; break; // shift arrow up
							case 'B': se_data.cury+=5; break; // shift arrow down
							case 'C': se_data.curx += (se_data.ispixelmode) ? 10 : 5; break; // shift arrow right
							case 'D': se_data.curx -= (se_data.ispixelmode) ? 10 : 5; break; // shift arrow left
							}
						} else if (c == '5') { // ctrl special char
							c = input_read(0, NULL); // read input
							switch(c) {
							case 'A': se_data.cury = 0; break; // ctrl arrow up
							case 'B': se_data.cury = se_data.height - 1; break; // ctrl arrow down
							case 'C': se_data.curx = se_data.width - 1; break; // ctrl arrow right
							case 'D': se_data.curx = 0; break; // ctrl arrow left
							}

						}
					} else {
						input_read(0, NULL); // read tilde
						switch(c) {
						case '5': se_switch_chars(); draw_term_clear(); se_redraw_all(); se_data.issaved = 0; break; // F5 // switch char
						case '7': se_save_wrapper(); break; // F6 // save
						case '8': // F7 // toggle auto cursor move
							se_data.isautocursormove = (se_data.isautocursormove) ? 0 : 1;
							break;
						case '9': // F8 // toggle pixel mode
							se_data.ispixelmode = (se_data.ispixelmode) ? 0 : 1;
							break;
						}
					}
				} else if (c == '2') {
					c = input_read(0, NULL); // read input
					input_read(0, NULL); // read tilde
					switch(c) {
					case '0': // F9 // redraw all
						draw_term_clear();
						se_redraw_all();
						break;
					}
				} else {
					switch(c) {
					case 'A': se_data.cury--; break; // arrow up
					case 'B': se_data.cury++; break; // arrow down
					case 'C': se_data.curx += (se_data.ispixelmode) ? 2 : 1; break; // arrow right
					case 'D': se_data.curx -= (se_data.ispixelmode) ? 2 : 1; break; // arrow left
					case 'Z': // shift TAB
						// go to prev window
						se_data.selectedwindow--;
						if (se_data.selectedwindow < 0)
							se_data.selectedwindow = 2;
						break;
					}
				}
			} else if (s != -1 && c == 'O') { // test if special char sequence for F1-F4
				c = input_read(0, NULL); // read input
				switch(c) {
				case 'P': se_help_menu(); draw_term_clear(); se_redraw_all(); break; // F1 // help screen
				case 'Q': se_settings_menu(); draw_term_clear(); se_redraw_all(); break; // F2 // settings
				case 'R': // F3 // block selection
					if (se_data.isblockselection) {
						se_data.isblockselection = 0;
					} else {
						se_data.isblockselection = 1;
						se_data.selx = se_data.curx;
						se_data.sely = se_data.cury;
					}
					break;
				case 'S': se_replace_chars(); draw_term_clear(); se_redraw_all(); se_data.issaved = 0; break; // F4 // replace char
				}

			} else if (s == 0) {
				// just pressed escape
				if (se_data.isblockselection) {
					se_data.isblockselection = 0;
				} else {
					running = se_quit_menu();
					draw_term_clear();
					se_redraw_all();
				}
			}
	
		} else {
			// some other character
			if ((isgraph(c) || c == ' ')) {
				if (se_data.isblockselection) {
					if (se_data.selectedwindow == 0 || se_is_hex(c)) {
						se_set_block_char(c);
						se_data.isblockselection = 0;
					}
				} else {
					se_set_char(c, se_data.curx, se_data.cury);
					if (se_data.ispixelmode)
						se_set_char(c, se_data.curx+1, se_data.cury);
					if (se_data.isautocursormove)
						se_data.curx += (se_data.ispixelmode) ? 2 : 1;
				}
				se_data.issaved = 0;
			} else if (c == '\t') { // Tab
				// go to next window
				se_data.selectedwindow++;
				if (se_data.selectedwindow > 2)
					se_data.selectedwindow = 0;
			} else if (c == '\0' || c == 22) { // Ctrl + Space or Ctrl + V // Block Selection
				if (se_data.isblockselection) {
					se_data.isblockselection = 0;
				} else {
					se_data.isblockselection = 1;
					se_data.selx = se_data.curx;
					se_data.sely = se_data.cury;
				}
			}

		}
		
		if (se_data.cury < 0)
			se_data.cury = 0;
		else if (se_data.cury >= se_data.height)
			se_data.cury = se_data.height -1;
		if (se_data.curx < 0)
			se_data.curx = 0;
		else if (se_data.curx >= se_data.width)
			se_data.curx = se_data.width -1;
		if (se_data.ispixelmode && se_data.curx % 2 != 0) {
			se_data.curx--;
		}

		se_set_sprite_in_lib();
		se_redraw_all();
		draw_flush();
	}


	input_destroy();
	draw_destroy(&origtermsettings); // destory draw environment

	draw_color_reset();
	se_print_sprite_as_c();
	se_free();

	return 0;
}


void se_help_menu() {
	const int optioncount = 14;
	const char * options[optioncount];
	options[ 0] = "(Shift+ or Ctrl+) Arrow keys | Cursor movement.";
	options[ 1] = "                         ESC | Exit";
	options[ 2] = "                (Shift+) Tab | Switch windows.";
	options[ 3] = "                          F1 | Show this help.";
	options[ 4] = "                          F2 | Open the settings menu.";
	options[ 5] = "                          F3 \\ (De-)Activate Block Selection.";
	options[ 6] = "                Ctrl + Space |    While in Block Selection any action is";
	options[ 7] = "                    Ctrl + v /    applied on that selection.";
	options[ 8] = "                          F4 | Replace every instance of one char with another.";
	options[ 9] = "                          F5 | Switch/Swap every instance of two chars.";
	options[10] = "                          F6 | Save/Write the file to disk.";
	options[11] = "                          F7 | Toggle auto cursor moving behaviour while typing.";
	options[12] = "                          F8 | Toggle Pixel mode. (every action is applied on two chars)";
	options[13] = "                          F9 | Redraw the entire screen.";
	
	draw_prompt_selection(-1, "===>>> Help / Controls <<<===", "Press any key to continue.", optioncount, options, '>', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);

	input_read(INPUT_FLAG_BLOCK, NULL);


}

void se_settings_menu_transparentbgcolora() {
	const int optioncount = 2;
	const char * options[optioncount];
	options[0] = "This option sets one of the colors used in ";
	options[1] = "the background of the sprite preview on the right.";
	
	draw_prompt_selection(-1, "===>>> Settings -> Set background color A. <<<===", "Press any key to set as new background color A.", optioncount, options, '>', COLOR_WHITE, COLOR_BLUE, COLOR_BRIGHT_BLACK, NULL);

	char c;
	c = input_read(INPUT_FLAG_BLOCK, NULL);
	if (se_is_hex(c)) {
		se_data.transparentbgcolora = c;
	}

}

void se_settings_menu_transparentbgcolorb() {
	const int optioncount = 2;
	const char * options[optioncount];
	options[0] = "This option sets one of the colors used in ";
	options[1] = "the background of the sprite preview on the right.";
	
	draw_prompt_selection(-1, "===>>> Settings -> Set background color B. <<<===", "Press any key to set as new background color B.", optioncount, options, '>', COLOR_WHITE, COLOR_BLUE, COLOR_BRIGHT_BLACK, NULL);

	char c;
	c = input_read(INPUT_FLAG_BLOCK, NULL);
	if (se_is_hex(c)) {
		se_data.transparentbgcolorb = c;
	}

}

void se_settings_menu_transparent() {
	const int optioncount = 0;
	const char * options[optioncount];
	
	draw_prompt_selection(-1, "===>>> Settings -> Set transparent. <<<===", "Press any key to set as new transparent.", optioncount, options, '>', COLOR_WHITE, COLOR_BLUE, COLOR_BRIGHT_BLACK, NULL);

	char c;
	c = input_read(INPUT_FLAG_BLOCK, NULL);
	if (isgraph(c) || c == ' ') {
		se_data.transparent = c;
		se_data.issaved = 0;
	}
}

void se_settings_menu() {

	static char bufa[256];
	sprintf(bufa, "Set transparent char. Current: '%c' ", se_data.transparent);
	static char bufb[256];
	sprintf(bufb, "Set background color A. Current: '%c' ", se_data.transparentbgcolora);
	static char bufc[256];
	sprintf(bufc, "Set background color B. Current: '%c' ", se_data.transparentbgcolorb);

	const int optioncount = 4;
	const char * options[optioncount];
	options[0] = bufa;
	options[1] = bufb;
	options[2] = bufc;
	options[3] = "Go back.";
	
	int selected = 0;
	while (1) { // infinite loop until a decision is made
		draw_term_clear();
		se_redraw_all();
		// draw the menu
		draw_prompt_selection(selected, "===>>> Settings <<<===", "Select an action.", optioncount, options, '>', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);

		switch(input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER | INPUT_FLAG_BLOCK, NULL)) { // read and handle input
		case 'd':
		case 's':
			selected++;
			if (selected > optioncount -1)
				selected = optioncount -1;
			break;
		case 'a':
		case 'w':
			selected--;
			if (selected < 0)
				selected = 0;
			break;
		case '\n':
		case ' ':
		case 'e': // confirm selection
			if (selected == 0)
				se_settings_menu_transparent();
			else if (selected == 1)
				se_settings_menu_transparentbgcolora();
			else if (selected == 2)
				se_settings_menu_transparentbgcolorb();
			else 
				return;

		case 'q':
		case '\033':
			return;
		}
	}


}

int se_save_wrapper() {
	if (se_save()) {
		return 1;
	} else {
		const int optioncount = 5;
		const char * options[optioncount];
		options[0] = "Failed to save the sprite.";
		options[1] = "File:";
		options[2] = se_data.spritefile;
		options[3] = "Error:";
		options[4] = strerror(errno);
		
		draw_prompt_selection(-1, "===>>> Save Failure <<<===", "Press any key to continue.", optioncount, options, ' ', 7, 1, 8, NULL);
		input_read(INPUT_FLAG_BLOCK, NULL); // wait for key press
		draw_term_clear();
		se_redraw_all();
		return 0;
	}
}

int se_save() {
	FILE* stream;
	if (!(stream = fopen(se_data.spritefile, "w")))
		return 0;
	
	// header
	if (fprintf(stream, "%s\n", SE_FILEHEADER_MAGICNUM) < 0) { fclose(stream); return 0; }
	if (fprintf(stream, "%s\n", SE_FILEHEADER_VERSION) < 0) { fclose(stream); return 0; }
	if (fprintf(stream, "%s\n", SE_FILEHEADER_COMMENTLINE) < 0) { fclose(stream); return 0; }

	// body

	// data header
	if (fprintf(stream, "width: $%d$\n", se_data.width) < 0) { fclose(stream); return 0; }
	if (fprintf(stream, "height: $%d$\n", se_data.height) < 0) { fclose(stream); return 0; }
	if (fprintf(stream, "transparent: '%c'\n", se_data.transparent) < 0) { fclose(stream); return 0; }
	
	// data
	int x, y;
	if (fprintf(stream, "%s\n", SE_FILEBODY_SEPERATOR) < 0) { fclose(stream); return 0; }
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			if (fprintf(stream, "%c", se_data.sprite[y * se_data.width + x]) < 0) { fclose(stream); return 0; }
		}
		if (fprintf(stream, "\n") < 0) { fclose(stream); return 0; }
	}
	if (fprintf(stream, "%s\n", SE_FILEBODY_SEPERATOR) < 0) { fclose(stream); return 0; }
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			if (fprintf(stream, "%c", se_data.fgcolors[y * se_data.width + x]) < 0) { fclose(stream); return 0; }
		}
		if (fprintf(stream, "\n") < 0) { fclose(stream); return 0; }
	}
	if (fprintf(stream, "%s\n", SE_FILEBODY_SEPERATOR) < 0) { fclose(stream); return 0; }
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			if (fprintf(stream, "%c", se_data.bgcolors[y * se_data.width + x]) < 0) { fclose(stream); return 0; }
		}
		if (fprintf(stream, "\n") < 0) { fclose(stream); return 0; }
	}
	if (fprintf(stream, "%s\n", SE_FILEBODY_SEPERATOR) < 0) { fclose(stream); return 0; }

	// footer
	if (fprintf(stream, "/%s\n", SE_FILEHEADER_MAGICNUM) < 0) { fclose(stream); return 0; }

	if (fclose(stream) == EOF)
		return 0;

	se_data.issaved = 1;
	return 1;
}

int se_load_expect_char(FILE* stream, char c) {
	int r = fgetc(stream);
	if (r == EOF)
		return -1;

	if (((char)r) != c)
		return -2;

	return 0;
}

int se_load_expect_line(FILE* stream, const char * str) {
	size_t len = strlen(str);
	int res;
	int i;
	for (i = 0; i < len; i++) {
		if ((res = se_load_expect_char(stream, str[i])) != 0) return res;
	}

	if ((res = se_load_expect_char(stream, '\n')) != 0) return res;

	return 0;
}

int se_load(char * file) {
	FILE* stream;
	if (!(stream = fopen(file, "r")))
		return 0;

	// header
	if (se_load_expect_line(stream, SE_FILEHEADER_MAGICNUM) != 0) { fclose(stream); return 0; }
	if (se_load_expect_line(stream, SE_FILEHEADER_VERSION) != 0) { fclose(stream); return 0; }
	if (se_load_expect_line(stream, SE_FILEHEADER_COMMENTLINE) != 0) { fclose(stream); return 0; }

	// body
	
	// data header
	if (fscanf(stream, "width: $%d$", &(se_data.width)) != 1) { fclose(stream); return 0; }
	if (se_load_expect_char(stream, '\n') != 0) { fclose(stream); return 0; }
	if (fscanf(stream, "height: $%d$", &(se_data.height)) != 1) { fclose(stream); return 0; }
	if (se_load_expect_char(stream, '\n') != 0) { fclose(stream); return 0; }

	// init with width and height
	if (!se_init(se_data.width, se_data.height)) {
		fclose(stream);
		return 0;
	}

	if (fscanf(stream, "transparent: '%c'", &(se_data.transparent)) != 1) { fclose(stream); se_free(); return 0; }
	if (se_load_expect_char(stream, '\n') != 0) { fclose(stream); se_free(); return 0; }


	// data
	int x, y;
	if (se_load_expect_line(stream, SE_FILEBODY_SEPERATOR) != 0) { fclose(stream); se_free(); return 0; }
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			if ((se_data.sprite[y * se_data.width + x] = (char)fgetc(stream)) == EOF) { fclose(stream); se_free(); return 0; }
		}
		if (se_load_expect_char(stream, '\n') != 0) { fclose(stream); se_free(); return 0; }
	}
	if (se_load_expect_line(stream, SE_FILEBODY_SEPERATOR) != 0) { fclose(stream); se_free(); return 0; }
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			if ((se_data.fgcolors[y * se_data.width + x] = (char)fgetc(stream)) == EOF) { fclose(stream); se_free(); return 0; }
		}
		if (se_load_expect_char(stream, '\n') != 0) { fclose(stream); se_free(); return 0; }
	}
	if (se_load_expect_line(stream, SE_FILEBODY_SEPERATOR) != 0) { fclose(stream); se_free(); return 0; }
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			if ((se_data.bgcolors[y * se_data.width + x] = (char)fgetc(stream)) == EOF) { fclose(stream); se_free(); return 0; }
		}
		if (se_load_expect_char(stream, '\n') != 0) { fclose(stream); se_free(); return 0; }
	}
	if (se_load_expect_line(stream, SE_FILEBODY_SEPERATOR) != 0) { fclose(stream); se_free(); return 0; }

	// footer
	if (se_load_expect_char(stream, '/') != 0) { fclose(stream); se_free(); return 0; }
	if (se_load_expect_line(stream, SE_FILEHEADER_MAGICNUM) != 0) { fclose(stream); se_free(); return 0; }


	if (fclose(stream) == EOF) {
		se_free();
		return 0;
	}

	if (!se_set_sprite_in_lib()) {
		se_free();
		return 0;
	}

	return 1;
}

int se_set_sprite_in_lib() {
	return draw_sprite_set(SE_SID, se_data.width, se_data.height, se_data.sprite, se_data.fgcolors, se_data.bgcolors, se_data.transparent);
}

int se_hex_to_col(char c) {
	if (c >= '0' && c <= '9')
		return c - '0';
	else 
		return tolower(c) - 'a' + 10;
}

int se_is_hex(char c) {
	c = tolower(c);
	return (c >= '0' && c <= '9') || (c >= 'a' && c <= 'f');
}

int se_init(int width, int height) {
	if (width <= 0 || height <= 0 || width > 10000 || height > 10000) {
		fprintf(stderr, "ERROR: se_init: width and height need to be between 1 and 10000 each!\n");
		return 0;
	}
	se_data.width = width;
	se_data.height = height;
	se_data.transparent = '.';
	se_data.transparentbgcolora = '2';
	se_data.transparentbgcolorb = '5';
	se_data.transparent = '.';
	se_data.selectedwindow = 0;
	se_data.curx = 0;
	se_data.cury = 0;
	se_data.isblockselection = 0;
	se_data.selx = 0;
	se_data.sely = 0;
	se_data.ispixelmode = 0;
	se_data.isautocursormove = 1;
	se_data.issaved = 0;
	size_t size = width*height;

	if ((se_data.sprite = malloc(sizeof(char) * (size+1))) == NULL) {
		fprintf(stderr, "ERROR: se_init: malloc failed! size: %ld \n", size);
		return 0;
	}
	if ((se_data.fgcolors = malloc(sizeof(int) * (size))) == NULL) {
		fprintf(stderr, "ERROR: se_init: malloc failed! size: %ld \n", size);
		free(se_data.sprite);
		return 0;
	}
	if ((se_data.bgcolors = malloc(sizeof(int) * (size))) == NULL) {
		fprintf(stderr, "ERROR: se_init: malloc failed! size: %ld \n", size);
		free(se_data.sprite);
		free(se_data.fgcolors);
		return 0;
	}

	int i;
	for (i = 0; i < size; i++) {
		se_data.sprite[i] = se_data.transparent;
		se_data.fgcolors[i] = '7';
		se_data.bgcolors[i] = '0';
	}
	se_data.sprite[size] = '\0';

	if (!draw_sprite_set(SE_SID_COLORS, 7, 19,"\
Colors:\
.+---+.\
.|000|.\
.|111|.\
.|222|.\
.|333|.\
.|444|.\
.|555|.\
.|666|.\
.|777|.\
.|888|.\
.|999|.\
.|AAA|.\
.|BBB|.\
.|CCC|.\
.|DDD|.\
.|EEE|.\
.|FFF|.\
.+---+.","\
7777777\
7777777\
7770777\
7771777\
7772777\
7773777\
7774777\
7775777\
7776777\
7777777\
7778777\
7779777\
777A777\
777B777\
777C777\
777D777\
777E777\
777F777\
7777777","\
0000000\
0444440\
0400040\
0400140\
0400240\
0400340\
0400440\
0400540\
0400640\
0400740\
0400840\
0400940\
0400A40\
0400B40\
0400C40\
0400D40\
0400E40\
0400F40\
0444440", '.')) return 0;

	return se_set_sprite_in_lib();
}

void se_free() {
	free(se_data.sprite);
	free(se_data.fgcolors);
	free(se_data.bgcolors);
}

void se_draw_raw_sprite() {
	int offx;
	int offy;

	offx = offset_x_g + (se_data.width + 3) * 0 + 1;
	offy = offset_y_g + (se_data.height + 3) * 0 + 1;

	draw_color_fg(COLOR_WHITE);
	draw_color_bg((se_data.selectedwindow == 0) ? COLOR_GREEN : COLOR_BLUE);
	draw_rect_outline(offx - 1, offy - 1, se_data.width + 2, se_data.height + 2, NULL);

	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	int x;
	int y;
	for (y = 0; y < se_data.height; y++) {
		draw_cursor_move(offx, y + offy);
		for (x = 0; x < se_data.width; x++) {
			putchar(se_data.sprite[y * se_data.width + x]);
		}
		draw_flush();
	}
	//draw_rect_fill(offx, offy, se_data.width, se_data.height, '#');
	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	draw_print_center(offx-1, offy -2, se_data.width+2, "Raw Sprite:", -1);
	draw_flush();
}
void se_draw_fgcolors() {
	int offx;
	int offy;

	offx = offset_x_g + (se_data.width + 3) * 0 + 1;
	offy = offset_y_g + (se_data.height + 3) * 1 + 1;

	draw_color_fg(COLOR_WHITE);
	draw_color_bg((se_data.selectedwindow == 1) ? COLOR_GREEN : COLOR_BLUE);
	draw_rect_outline(offx - 1, offy - 1, se_data.width + 2, se_data.height + 2, NULL);

	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	int x;
	int y;
	int col;
	for (y = 0; y < se_data.height; y++) {
		draw_cursor_move(offx, y + offy);
		for (x = 0; x < se_data.width; x++) {
			col = se_hex_to_col(se_data.fgcolors[y * se_data.width + x]);
			draw_color_fg(col);
			draw_color_bg((col == 0) ? COLOR_WHITE : COLOR_BLACK);
			putchar(se_data.fgcolors[y * se_data.width + x]);
		}
		draw_flush();
	}
	//draw_rect_fill(offx, offy, se_data.width, se_data.height, '#');
	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	draw_print_center(offx-1, offy -2, se_data.width+2, "FG Colors:", -1);
	draw_flush();
}
void se_draw_bgcolors() {
	int offx;
	int offy;

	offx = offset_x_g + (se_data.width + 3) * 0 + 1;
	offy = offset_y_g + (se_data.height + 3) * 2 + 1;

	draw_color_fg(COLOR_WHITE);
	draw_color_bg((se_data.selectedwindow == 2) ? COLOR_GREEN : COLOR_BLUE);
	draw_rect_outline(offx - 1, offy - 1, se_data.width + 2, se_data.height + 2, NULL);

	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	int x;
	int y;
	int col;
	for (y = 0; y < se_data.height; y++) {
		draw_cursor_move(offx, y + offy);
		for (x = 0; x < se_data.width; x++) {
			col = se_hex_to_col(se_data.bgcolors[y * se_data.width + x]);
			draw_color_fg((col == 0) ? COLOR_WHITE : COLOR_BLACK);
			draw_color_bg(col);
			putchar(se_data.bgcolors[y * se_data.width + x]);
		}
		draw_flush();
	}
	//draw_rect_fill(offx, offy, se_data.width, se_data.height, '#');
	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	draw_print_center(offx-1, offy -2, se_data.width+2, "BG Colors:", -1);
	draw_flush();
}


void se_draw_sprite() {
	int offx;
	int offy;

	offx = offset_x_g + (se_data.width + 3) * 1 + 2;
	offy = offset_y_g + (se_data.height + 3) * 1 + 1;
	
	int cola = se_hex_to_col(se_data.transparentbgcolora);
	int colb = se_hex_to_col(se_data.transparentbgcolorb);

	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLUE);
	draw_rect_outline(offx - 1, offy - 1, se_data.width + 2, se_data.height + 2, NULL);

	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	int x;
	int y;
	int k = 0;
	for (y = 0; y < se_data.height; y++) {
		draw_cursor_move(offx, y + offy);
		k = (y % 2) ? 0 : 1;
		for (x = 0; x < se_data.width; x++) {
			draw_color_bg((k % 2) ? cola : colb);
			putchar(' ');
			k++;
		}
		draw_flush();
	}

	draw_sprite(offx, offy, SE_SID);
	draw_flush();
	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	draw_print_center(offx-1, offy -2, se_data.width+2, "Result:", -1);
	draw_flush();
}

void se_draw_cursor(int window, int x, int y) {

	int offx;
	int offy;
	int col;

	offx = offset_x_g + (se_data.width + 3) * 0 + 1;
	offy = offset_y_g + (se_data.height + 3) * window + 1;

	draw_cursor_move(offx + x, offy + y);
	switch (window) {
		case 0: // raw sprite
			draw_color_fg(COLOR_BLACK);
			draw_color_bg(COLOR_WHITE);
			putchar(se_data.sprite[y * se_data.width + x]);
			break;
		case 1: // fg colors
			col = se_hex_to_col(se_data.fgcolors[y * se_data.width + x]);
			draw_color_bg(col);
			draw_color_fg((col == 0) ? COLOR_WHITE : COLOR_BLACK);
			putchar(se_data.fgcolors[y * se_data.width + x]);
			break;
		case 2: // bg colors
			col = se_hex_to_col(se_data.bgcolors[y * se_data.width + x]);
			draw_color_bg((col == 0) ? COLOR_WHITE : COLOR_BLACK);
			draw_color_fg(col);
			putchar(se_data.bgcolors[y * se_data.width + x]);
			break;
	}
	//draw_flush();
}

void se_redraw_all() {
	int cols, rows;
	cols = draw_term_get_cols();
	rows = draw_term_get_rows();
	offset_x_g = (cols/2) - (((se_data.width + 3) * 2 ) / 2) + 1 + 5;
	offset_y_g = (rows/2) - (((se_data.height + 3) * 3) / 2) + 1;

	draw_color_reset(); // reset colors
	//draw_term_clear();


	draw_sprite(offset_x_g - 10, rows / 2 - 9, SE_SID_COLORS);

	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_BLACK);
	draw_cursor_move(1,rows);
	draw_color_fg(COLOR_BLUE); printf(" ESC");
	draw_color_fg(COLOR_WHITE);
	/*if (se_data.isblockselection)
		printf(" Cancel Block Selection");
	else*/
		printf(" Exit");
	
	draw_color_fg(COLOR_BLUE); printf(" TAB");
	draw_color_fg(COLOR_WHITE); printf(" Switch Window");
	draw_color_fg(COLOR_BLUE); printf(" F1");
	draw_color_fg(COLOR_WHITE); printf(" Help");
	draw_color_fg(COLOR_BLUE); printf(" F2");
	draw_color_fg(COLOR_WHITE); printf(" Settings");
	draw_color_fg((se_data.isblockselection) ? COLOR_GREEN : COLOR_BLUE); printf(" F3");
	draw_color_fg(COLOR_WHITE); printf(" Block Selection");
	draw_color_fg(COLOR_BLUE); printf(" F4");
	draw_color_fg(COLOR_WHITE); printf(" Replace Chars");
	draw_color_fg(COLOR_BLUE); printf(" F5");
	draw_color_fg(COLOR_WHITE); printf(" Switch Chars");
	draw_color_fg((se_data.issaved) ? COLOR_BLUE : COLOR_RED); printf(" F6");
	draw_color_fg(COLOR_WHITE); printf(" Save");
	draw_color_fg((se_data.isautocursormove) ? COLOR_GREEN : COLOR_BLUE); printf(" F7");
	draw_color_fg(COLOR_WHITE); printf(" Toggle Auto Cursor Move");
	draw_color_fg((se_data.ispixelmode) ? COLOR_GREEN : COLOR_BLUE); printf(" F8");
	draw_color_fg(COLOR_WHITE); printf(" Toggle Pixel Mode");
	draw_color_fg(COLOR_BLUE); printf(" F9");
	draw_color_fg(COLOR_WHITE); printf(" Redraw Screen");
	//printf("                   ");

	static char buf[1024];
	sprintf(buf, " %8s%15s%12s Width: %3d  Height: %3d   Cursor: (%2d,%2d) Transparent: '%c' ",(se_data.ispixelmode) ? "[PIXEL] " : "",(se_data.isautocursormove) ? "[AUT CUR MOVE] " : "",(se_data.isblockselection) ? "[BLOCK SEL] " : "", se_data.width, se_data.height, se_data.curx, se_data.cury, se_data.transparent);
	//sprintf(buf, " Term: rows: %3d cols: %3d %8s%15s%12s Width: %3d  Height: %3d   Cursor: (%2d,%2d) ",rows,cols,(se_data.ispixelmode) ? "[PIXEL] " : "",(se_data.isautocursormove) ? "[AUT CUR MOVE] " : "",(se_data.isblockselection) ? "[BLOCK SEL] " : "", se_data.width, se_data.height, se_data.curx, se_data.cury);
	draw_color_bg(7);
	draw_color_fg(0);
	draw_print_center(1,rows-1, cols, buf, -1);

	sprintf(buf, "   SPRITE EDITOR  %4s        %s%s  %s ",SE_VERSION, se_data.spritefile, (se_data.issaved) ? "   " : "[+]", (se_data.programaction == SE_ACTION_NEW) ? "[NEW]" : "   ");
	draw_color_bg(7);
	draw_color_fg(0);
	draw_print_center(1,1, cols, buf, -1);

	se_draw_raw_sprite();
	se_draw_fgcolors();
	se_draw_bgcolors();
	
	se_draw_cursor(0, se_data.curx, se_data.cury);
	se_draw_cursor(1, se_data.curx, se_data.cury);
	se_draw_cursor(2, se_data.curx, se_data.cury);
	if (se_data.ispixelmode) {
		se_draw_cursor(0, se_data.curx + 1, se_data.cury);
		se_draw_cursor(1, se_data.curx + 1, se_data.cury);
		se_draw_cursor(2, se_data.curx + 1, se_data.cury);
	}
	int x,y;
	int startx, starty, endx, endy;
	if (se_data.isblockselection) {
		if (se_data.curx < se_data.selx) {
			startx = se_data.curx;
			endx   = se_data.selx;
		} else {
			startx = se_data.selx;
			endx   = se_data.curx;
		}
		if (se_data.cury < se_data.sely) {
			starty = se_data.cury;
			endy   = se_data.sely;
		} else {
			starty = se_data.sely;
			endy   = se_data.cury;
		}
		if (se_data.ispixelmode) {
			endx++;
		}
		for (x = startx; x <= endx; x++) {
			for (y = starty; y <= endy; y++) {
				se_draw_cursor(0, x, y);
				se_draw_cursor(1, x, y);
				se_draw_cursor(2, x, y);
				/*draw_cursor_move(1,1);
				printf("curx: %3d selx: %3d startx: %3d endx: %3d x: %3d\r\n", se_data.curx, se_data.selx, startx, endx, x);
				printf("cury: %3d sely: %3d starty: %3d endy: %3d y: %3d\r\n", se_data.cury, se_data.sely, starty, endy, y);
				usleep(1000000);*/

			}
			draw_flush();

		}

	}

	//se_draw_sprite_background();
	se_draw_sprite();
	draw_flush();
}

void se_print_sprite_as_c() {
	/*
	 *
	 * 	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_PAWN, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
	 * ................\
	 * ................\
	 * ......._........\
	 * ......(_).......\
	 * .....(___)......\
	 * ....._|_|_......\
	 * ....(_____).....\
	 * ..../__P__\\.....","\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF\
	 * FFFFFFFFFFFFFFFF","\
	 * 0000000000000000\
	 * 0000000000000000\
	 * 0000000000000000\
	 * 0000000000000000\
	 * 0000000000000000\
	 * 0000000000000000\
	 * 0000000000000000\
	 * 0000000000000000", '.')) return 0;
	 */


	int y;
	int x;
	char c;
	printf("\tif (!draw_sprite_set(<SID>, %d, %d,\"\\\n", se_data.width, se_data.height);
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			c = se_data.sprite[y * se_data.width + x];
			if (c == '\\' || c == '\"')
				putchar('\\');
			putchar(c);
		}
		if (y != se_data.height -1) {
			putchar('\\');
			putchar('\n');
		}
	}
	printf("\",\"\\\n");
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			c = se_data.fgcolors[y * se_data.width + x];
			putchar(c);
		}
		if (y != se_data.height -1) {
			putchar('\\');
			putchar('\n');
		}
	}
	printf("\",\"\\\n");
	for (y = 0; y < se_data.height; y++) {
		for (x = 0; x < se_data.width; x++) {
			c = se_data.bgcolors[y * se_data.width + x];
			putchar(c);
		}
		if (y != se_data.height -1) {
			putchar('\\');
			putchar('\n');
		}
	}
	printf("\", '%c')) return 0;\n", se_data.transparent);

}

void se_set_char(char c, int x, int y) {
	if (se_data.selectedwindow == 0)
		se_data.sprite[y * se_data.width + x] = c;
	else if (se_data.selectedwindow == 1 && se_is_hex(c))
		se_data.fgcolors[y * se_data.width + x] = toupper(c);
	else if (se_data.selectedwindow == 2 && se_is_hex(c))
		se_data.bgcolors[y * se_data.width + x] = toupper(c);

}

void se_set_block_char(char c) {
	int x,y;
	int startx, starty, endx, endy;
	if (se_data.curx < se_data.selx) {
		startx = se_data.curx;
		endx   = se_data.selx;
	} else {
		startx = se_data.selx;
		endx   = se_data.curx;
	}
	if (se_data.cury < se_data.sely) {
		starty = se_data.cury;
		endy   = se_data.sely;
	} else {
		starty = se_data.sely;
		endy   = se_data.cury;
	}
	if (se_data.ispixelmode) {
		endx++;
	}
	for (x = startx; x <= endx; x++) {
		for (y = starty; y <= endy; y++) {
			se_set_char(c,x,y);
		}
	}
}

int se_quit_menu() {
	const int optioncount = 3;
	const char * options[optioncount];
	options[0] = "1. Save and Exit.";
	options[1] = "2. Exit.";
	options[2] = "3. Go back.";
	
	int selected = 0;
	while (1) { // infinite loop until a decision is made
		// draw the menu
		draw_prompt_selection(selected, "===>>> Quit Menu <<<===", "Select an action.", optioncount, options, '>', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);

		switch(input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER | INPUT_FLAG_BLOCK, NULL)) { // read and handle input
		case 'd':
		case 's':
			selected++;
			if (selected > optioncount -1)
				selected = optioncount -1;
			break;
		case 'a':
		case 'w':
			selected--;
			if (selected < 0)
				selected = 0;
			break;
		case '\n':
		case ' ':
		case 'e': // confirm selection
			if (selected == 0)
				return se_save_wrapper() ? 0 : 1;
			return (selected == 2) ? 1 : 0;
		case '1':
			se_save_wrapper();
			return 0;
		case '2':
			return 0;
		case 'q':
		case '\033':
		case '3': return 1;
		}
	}

}

void se_replace_chars() {
	char a;
	char b;
	//const int optioncount = 0;
	//const char * options[optioncount];
	int cols, rows;
	rows = draw_term_get_rows();
	cols = draw_term_get_cols();
	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_GREEN);
	draw_print_center(1,rows-2, cols, (se_data.isblockselection) ? " [REPLACE CHARS (IN SELECTION)] Set first char (to replace).    " : " [REPLACE CHARS] Set first char (to replace).    ", -1);
	draw_flush();

	//draw_prompt_selection(-1, "===>>> Replace Chars <<<===", "Set first char (to replace).", optioncount, options, '>', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);
	a = input_read(INPUT_FLAG_BLOCK, NULL);
	if (!(isgraph(a) || a == ' ')) {
		return;
	}
	draw_print_center(1,rows-2, cols, (se_data.isblockselection) ? " [REPLACE CHARS (IN SELECTION)] Set second char (replace with). " : " [REPLACE CHARS] Set second char (replace with). ", -1);
	draw_flush();
	//draw_prompt_selection(-1, "===>>> Replace Chars <<<===", "Set second char (replace with).", optioncount, options, '>', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);
	b = input_read(INPUT_FLAG_BLOCK, NULL);
	if (!(isgraph(b) || b == ' ')) {
		return;
	}
	int x,y;
	int startx, starty, endx, endy;
	if (se_data.isblockselection) {
		if (se_data.curx < se_data.selx) {
			startx = se_data.curx;
			endx   = se_data.selx;
		} else {
			startx = se_data.selx;
			endx   = se_data.curx;
		}
		if (se_data.cury < se_data.sely) {
			starty = se_data.cury;
			endy   = se_data.sely;
		} else {
			starty = se_data.sely;
			endy   = se_data.cury;
		}
		if (se_data.ispixelmode) {
			endx++;
		}
	} else {
		startx = 0;
		starty = 0;
		endx = se_data.width -1;
		endy = se_data.height -1;
	}
	
	char * field;

	if (se_data.selectedwindow == 0)
		field = se_data.sprite;
	else if (se_data.selectedwindow == 1 && se_is_hex(a) && se_is_hex(b)) {
		field = se_data.fgcolors;
		a = toupper(a);
		b = toupper(b);
	} else if (se_data.selectedwindow == 2 && se_is_hex(a) && se_is_hex(b)) {
		field = se_data.bgcolors;
		a = toupper(a);
		b = toupper(b);
	} else {
		return;
	}

	for (x = startx; x <= endx; x++) {
		for (y = starty; y <= endy; y++) {
			if (field[y * se_data.width + x] == a)
				field[y * se_data.width + x] = b;
		}
	}
}

void se_switch_chars() {
	char a;
	char b;
	//const int optioncount = 0;
	//const char * options[optioncount];
	int cols, rows;
	rows = draw_term_get_rows();
	cols = draw_term_get_cols();
	draw_color_fg(COLOR_WHITE);
	draw_color_bg(COLOR_CYAN);
	draw_print_center(1,rows-2, cols, (se_data.isblockselection) ? " [SWITCH CHARS (IN SELECTION)] Set first char.  " : " [SWITCH CHARS] Set first char.  ", -1);
	draw_flush();
	//draw_prompt_selection(-1, "===>>> Switch Chars <<<===", "Set first char.", optioncount, options, '>', 7, 5, 0, NULL);
	a = input_read(INPUT_FLAG_BLOCK, NULL);
	if (!(isgraph(a) || a == ' ')) {
		return;
	}
	draw_print_center(1,rows-2, cols, (se_data.isblockselection) ? " [SWITCH CHARS (IN SELECTION)] Set second char. " : " [SWITCH CHARS] Set second char. ", -1);
	draw_flush();
	//draw_prompt_selection(-1, "===>>> Switch Chars <<<===", "Set second char.", optioncount, options, '>', 7, 5, 0, NULL);
	b = input_read(INPUT_FLAG_BLOCK, NULL);
	if (!(isgraph(b) || b == ' ')) {
		return;
	}
	int x,y;
	int startx, starty, endx, endy;
	if (se_data.isblockselection) {
		if (se_data.curx < se_data.selx) {
			startx = se_data.curx;
			endx   = se_data.selx;
		} else {
			startx = se_data.selx;
			endx   = se_data.curx;
		}
		if (se_data.cury < se_data.sely) {
			starty = se_data.cury;
			endy   = se_data.sely;
		} else {
			starty = se_data.sely;
			endy   = se_data.cury;
		}
		if (se_data.ispixelmode) {
			endx++;
		}
	} else {
		startx = 0;
		starty = 0;
		endx = se_data.width -1;
		endy = se_data.height -1;
	}
	
	char * field;

	if (se_data.selectedwindow == 0)
		field = se_data.sprite;
	else if (se_data.selectedwindow == 1 && se_is_hex(a) && se_is_hex(b)) {
		field = se_data.fgcolors;
		a = toupper(a);
		b = toupper(b);
	} else if (se_data.selectedwindow == 2 && se_is_hex(a) && se_is_hex(b)) {
		field = se_data.bgcolors;
		a = toupper(a);
		b = toupper(b);
	} else {
		return;
	}

	for (x = startx; x <= endx; x++) {
		for (y = starty; y <= endy; y++) {
			if (field[y * se_data.width + x] == a)
				field[y * se_data.width + x] = b;
			else if (field[y * se_data.width + x] == b)
				field[y * se_data.width + x] = a;
		}
	}
}




