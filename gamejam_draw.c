#include "gamejam.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <unistd.h>
#include <errno.h>
#include <ctype.h>
#include <sys/ioctl.h>

/**
 * @brief Some simple drawing helper functions + Sprite support.
 *
 *
 */




// the default border characters
struct draw_border draw_border_default = {
	'-',
	'|',
	'-',
	'|',
	'+',
	'+',
	'+',
	'+'
};


struct draw_internal draw_internal_data;

/**
 * @brief Initializes the draw environment.
 *
 * @param[in] spritecount the number of sprites you want to reserver space for
 * @param[in,out] origtermsettings where the current terminal settings should be stored
 *
 * @note You need to call draw_destroy() after draw_init() before exiting the program!
 * @note This function should only be called once at the beginning of the program or after a draw_destroy() call.
 * @note This will flush the draw buffer.
 *
 * Example:
 * @code
 * 	struct termios origtermsettings; // original terminal settings are stored in here (you don't need to think too much about this, it's just needed by draw_init() and draw_destroy())
 * 	if (!draw_init(16, &origtermsettings)) {// init the draw environment
 *		// an error occured
 * 	}
 *
 * 	// code in here is inside a draw environment
 *
 * 	draw_destroy(&origtermsettings); // destory the draw environment
 * @endcode
 *
 */
int draw_init(int spritecount, struct termios * origtermsettings) {
	tcgetattr(STDIN_FILENO, origtermsettings); // backup terminal settings

	// set new terminal settings
	struct termios t;
	tcgetattr(STDIN_FILENO, &t);
        t.c_iflag &= ~(IGNBRK | PARMRK | ISTRIP
              | IGNCR | INLCR | IXON); // unset
	t.c_iflag |= BRKINT | ICRNL | INLCR; // set
        t.c_oflag &= ~(OPOST | ONLRET);
        //t.c_oflag |= ONLCR | OCRNL;
        t.c_lflag &= ~(ECHO | ECHONL | ICANON | ISIG | IEXTEN);
        t.c_lflag |= ISIG;
        t.c_cflag &= ~(CSIZE | PARENB);
        t.c_cflag |= CS8;

        //t.c_lflag &= ~(ECHO | ICANON);
	tcsetattr(STDIN_FILENO, TCSANOW, &t);


	spritecount++; // making sure sid can be equal to spritecount // allowing indexes to start at 1
	draw_internal_data.spritecount = spritecount;
	if ((draw_internal_data.sprites = malloc(sizeof(struct draw_internal_sprite) * (spritecount))) == NULL) {
		fprintf(stderr, "ERROR: in draw_init(%d): Couldn't alloc sprites: %s\n", spritecount, strerror(errno));
		return 0;
	}

	int i;
	for (i = 0; i < spritecount;i++)  {
		draw_sprite_set(i,0,0,NULL,NULL,NULL,' ');
	}

	draw_color_reset();
	//draw_term_echo_hide();
	draw_cursor_hide();

	draw_term_get_cols(); // init termcols and termrows

	/* Save current stdout for use later (alternate screen)*/
	printf("\e[?1049h");

	//draw_term_clear();
	fflush(stderr);
	draw_flush();

	return 1;
}

/**
 * @brief Destroys the draw environment.
 *
 * @note Must be called after draw_init()!
 * @param[in,out] origtermsettings original terminal settings
 *
 */
void draw_destroy(struct termios * origtermsettings) {
	tcsetattr(STDIN_FILENO, TCSANOW, origtermsettings); // restore original terminal settings

	/* Restore stdout */
	printf("\e[?1049l");

	draw_cursor_show();
	//draw_term_echo_show();
	draw_color_reset();

	struct draw_internal_sprite * cur;
	int i;
	for (i = 0; i < draw_internal_data.spritecount;i++)  {
		cur = &(draw_internal_data.sprites[i]);
		free(cur->sprite);
		free(cur->fgcolors);
		free(cur->bgcolors);
	}
	free(draw_internal_data.sprites);
	fflush(stderr);
	draw_flush();
}

/**
 * @brief Gets the current number of columns the terminal has.
 *
 * @note Also updates draw_internal_data cached terminal resolution.
 */
int  draw_term_get_cols() {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    draw_internal_data.termcols = w.ws_col;
    draw_internal_data.termrows = w.ws_row;
    return w.ws_col;
}

/**
 * @brief Gets the current number of rows the terminal has.
 *
 * @note Also updates draw_internal_data cached terminal resolution.
 */
int  draw_term_get_rows() {
    struct winsize w;
    ioctl(STDOUT_FILENO, TIOCGWINSZ, &w);
    draw_internal_data.termcols = w.ws_col;
    draw_internal_data.termrows = w.ws_row;
    return w.ws_row;
}

/**
 * @brief Enable displaying typing echo in the terminal.
 *
 */
void draw_term_echo_show() {
	int fd = STDIN_FILENO;
	struct termios t;
	tcgetattr(fd, &t);
	t.c_lflag |= ECHO; // set
	//t.c_lflag |= ICANON; // enable buffer for getchar
	tcsetattr(fd, TCSANOW, &t);
}

/**
 * @brief Disable displaying typing echo in the terminal.
 *
 */
void draw_term_echo_hide() {
	int fd = STDIN_FILENO;
	struct termios t;
	tcgetattr(fd, &t);
	t.c_lflag &= ~ECHO; // unset
	//t.c_lflag &= ~(ICANON); // disable buffer for getchar
	tcsetattr(fd, TCSANOW, &t);
}

/**
 * @brief Prints a given string at a given position with a maximum width.
 *
 * @param[in] x     the x-coordinate
 * @param[in] y     the y-coordinate
 * @param[in] width the maximum width
 * @param[in] str   the string
 */
void draw_print_fixedwidth(int x, int y, int width, const char * str) {
	if (str == NULL)
		return;
	draw_cursor_move(x,y);
	int i = 0;
	char c;
	while (i < width && (c = str[i]) != '\0' ) {
		putchar(c);
		i++;
	}
}

/**
 * @brief Prints a given string in the horizontal center of a box defined by x, y and width (height is 1).
 *
 * @note If the string is longer than the width, it will be cut off.
 *
 * @param[in] x     the x-coordinate
 * @param[in] y     the y-coordinate
 * @param[in] width the width
 * @param[in] str   the string
 * @param[in] len   the length to be used of the string (-1: use strlen)
 */
void draw_print_center(int x, int y, int width, const char * str, int len) {
	if (str == NULL)
		return;
	if (len < 0)
		len = strlen(str);
	int printoffset = 0;
	int printx = x + (width / 2) - (len / 2);
	if (printx < x) {
		printoffset = x-printx;
		printx = x;
	}
	draw_cursor_move(printx,y);
	//printf("HELLO WORLD! printx: %d x: %d width: %d len: %d poffset: %d", printx,x,width,len,printoffset);
	int i;
	int max = printoffset + width;
	for (i = printoffset; i < max && i < len; i++)
		putchar(str[i]);


}

/**
 * @brief Prints a given string in as a 'tickertext' in a box defined by x, y and width (height is 1).
 *
 * @note If the string is longer than the width, it will be cut off.
 * @note If the string is shorter than the width, it will be repeated.
 * @note offset can be ANY integer value and thus can be used as a 'time index' for the ticker.
 *
 * @param[in] x      the x-coordinate
 * @param[in] y      the y-coordinate
 * @param[in] width  the width
 * @param[in] offset the offset of the ticker (you need to increment this value)
 * @param[in] str    the string
 * @param[in] space  the number of spaces between repetitions (0: no space)
 */
void draw_print_tickertext(int x, int y, int width, int offset, const char * str, int space) {
	if (str == NULL)
		return;
	draw_cursor_move(x,y);
	int len = strlen(str);
	int i;
	int k = offset % len;
	for (i = 0; i < width; i++) {
		if (k >= len)
			k = -space;
		putchar((k < 0) ? ' ' : str[k]);
		k++;
	}

}

/**
 * @brief Draws a selection prompt in the center of the terminal.
 *
 * @note The size of the prompt will always try to fit the content.
 * @note This will flush the draw buffer.
 * @note The colors are numbers from 0 to 15. See the Colors section in gamejam.h.
 *
 * @param[in] selected      the currently selected item (0-index based)
 * @param[in] title         the title (optional, use NULL to disable)
 * @param[in] subtitle      the subtitle (optional, use NULL to disable)
 * @param[in] optioncount   the number of strings in options
 * @param[in] options       the options, an array of strings with optioncount elements
 * @param[in] selectionchar the char to be displayed as the cursor
 * @param[in] fgcolor       the text color
 * @param[in] bgcolor       the background color
 * @param[in] shadowcolor   the color of the shadow of the box
 * @param[in] border        a draw_border struct describing the border characters of the box
 *
 */
void draw_prompt_selection(int selected, const char * title, const char * subtitle, int optioncount, const char ** options, char selectionchar, int fgcolor, int bgcolor, int shadowcolor, struct draw_border * border) {
	int i;
	size_t maxlen = 0;
	size_t tmp;
	for (i = 0; i < optioncount; i++) {
		tmp = strlen(options[i]);
		if (tmp > maxlen)
			maxlen = tmp;
	}
	if (title != NULL) {
		tmp = strlen(title);
		if (tmp > maxlen)
			maxlen = tmp;
	}
	if (subtitle != NULL) {
		tmp = strlen(subtitle);
		if (tmp > maxlen)
			maxlen = tmp;
	}
		
	int width = 2 + 2 + maxlen + 2;
	int height = optioncount + 2 + ((title != NULL) ? 1 : 0) + ((subtitle != NULL) ? 1 : 0);
	int x = (draw_term_get_cols()/2) - (width / 2) + 1;
	int y = (draw_term_get_rows()/2) - (height / 2) + 1;

	draw_shadowed_box(x,y,width,height,fgcolor,bgcolor,shadowcolor,border);
	y++;
	if (title != NULL) {
		draw_print_center(x+1,y,width -2,title, -1);
		y++;
	}
	for (i = 0; i < optioncount; i++) {
		if (i != selected) {
			draw_color_fg(fgcolor);
			draw_color_bg(bgcolor);
		} else {
			draw_cursor_move(x + 2, y);
			putchar(selectionchar);
			draw_color_fg(bgcolor);
			draw_color_bg(fgcolor);
		}
		draw_print_fixedwidth(x + 4, y, width - 6, options[i]);
		y++;
	}

	draw_color_fg(fgcolor);
	draw_color_bg(bgcolor);
	if (subtitle != NULL) {
		draw_print_center(x+1,y,width -2,subtitle, -1);
	}
	draw_flush();
}

/**
 * @brief Fills a rectangular with a character c.
 *
 * @param[in] x      the x-coordinate
 * @param[in] y      the y-coordinate
 * @param[in] width  the width
 * @param[in] height the height
 * @param[in] c      the character
 *
 * Example:
 * @code
 * 	draw_rect_fill(1,1,draw_term_get_cols(), draw_term_get_rows(), '#'); // fills entire terminal
 * @endcode
 */
void draw_rect_fill(int x, int y, int width, int height, char c) {
	int i;
	for (i = y; i < y+height; i++)
		draw_line_hori(x,i,width,c);
}

/**
 * @brief Draws an outline for a rectangular with a given border.
 *
 * @note The resulting size will be EXACTLY the same as with draw_rect_fill().
 *
 * @param[in] x      the x-coordinate
 * @param[in] y      the y-coordinate
 * @param[in] width  the width
 * @param[in] height the height
 * @param[in] border the border (NULL: use default border)
 *
 * Example:
 * @code 
 * 	draw_rect_outline(1,1,draw_term_get_cols(), draw_term_get_rows(), NULL); // creates a border around the terminal
 * @endcode
 */
void draw_rect_outline(int x, int y, int width, int height, struct draw_border * border) {
	if (border == NULL)
		border = &draw_border_default;
	width--;
	height--;
	draw_line_hori(x,y,width,border->n);
	draw_line_hori(x,y+height,width,border->s);
	draw_line_vert(x,y,height,border->w);
	draw_line_vert(x+width,y,height,border->e);
	draw_cursor_move(x,y); putchar(border->nw);
	draw_cursor_move(x+width,y); putchar(border->ne);
	draw_cursor_move(x,y+height); putchar(border->sw);
	draw_cursor_move(x+width,y+height); putchar(border->se);
}


/**
 * @brief Draws a shadow on the right and bottom sides of a rectangular.
 *
 * @note This is recommended to be combined with draw_rect_fill() or draw_rect_outline().
 * @note The shadow will be drawn OUTSIDE of what something like draw_rect_fill() or draw_rect_outline() would produce.
 *
 * @param[in] x      the x-coordinate
 * @param[in] y      the y-coordinate
 * @param[in] width  the width
 * @param[in] height the height
 * @param[in] c      the character to be used for the shadow
 *
 * Example:
 * @code
 * 	draw_rect_outline(10,10,32,18, NULL);
 * 	draw_color_bg(COLOR_BRIGHT_BLACK); // color of the shadow // COLOR_BLACK might also be a nice color for a shadow
 * 	draw_color_fg(COLOR_BRIGHT_BLACK);
 * 	draw_rect_shadow(10,10,21,18, ' ');
 * @endcode
 */
void draw_rect_shadow(int x, int y, int width, int height, char c) {
	draw_line_hori(x+1,y+height,width,c);
	draw_line_vert(x+width,y+1,height-1,c);
}

/**
 * @brief Combines draw_rect_fill(), draw_rect_outline() and draw_rect_shadow() to create a simple shadowed box.
 *
 * @note This will flush the draw buffer.
 * @note The colors are numbers from 0 to 15. See the Colors section in gamejam.h.
 *
 * @param[in] x           the x-coordinate
 * @param[in] y           the y-coordinate
 * @param[in] width       the width of the box
 * @param[in] height      the height of the box
 * @param[in] fgcolor     the text color
 * @param[in] bgcolor     the background color
 * @param[in] shadowcolor the color of the shadow of the box
 * @param[in] border      a draw_border struct describing the border characters of the box
 */
void draw_shadowed_box(int x, int y, int width, int height, int fgcolor, int bgcolor, int shadowcolor, struct draw_border * border) {
	draw_color_bg(shadowcolor);
	draw_color_fg(shadowcolor);
	draw_rect_shadow(x,y,width,height,' ');
	draw_color_bg(bgcolor);
	draw_rect_fill(x,y,width,height,' ');
	draw_color_fg(fgcolor);
	draw_rect_outline(x,y,width,height,border);
	draw_flush();
}

/**
 * @brief Draws a vertical line.
 *
 * @note The line is drawn downwards to positive y.
 *
 * @param[in] x           the x-coordinate
 * @param[in] y           the y-coordinate
 * @param[in] height      the height
 * @param[in] c           the character
 */
void draw_line_vert(int x, int y, int height, char c) {
	int i;
	for (i = y; i < y+height; i++) {
		draw_cursor_move(x,i);
		putchar(c);
	}
}

/**
 * @brief Draws a horizontal line.
 *
 * @note The line is drawn to the right to positive x.
 *
 * @param[in] x           the x-coordinate
 * @param[in] y           the y-coordinate
 * @param[in] width       the width
 * @param[in] c           the character
 */
void draw_line_hori(int x, int y, int width, char c) {
	draw_cursor_move(x,y);
	int i;
	for (i = 0; i < width; i++)
		putchar(c);
}


/**
 * @brief Sets a sprite.
 *
 * @param[in] sid         the sprite id (0 <= sid <= spritecount)
 * @param[in] width       the width of the sprite
 * @param[in] height      the height of the sprite
 * @param[in] sprite      the text of the sprite as an one-dimensional array accessed via [x + y*width]
 * @param[in] fgcolors    the text color for each character [x + y*width] (same as sprite)
 * @param[in] bgcolors    the background color for each character [x + y*width] (same as sprite)
 * @param[in] transparent the transparent character (in sprite) will NOT be drawn
 *
 * @note The strlen of sprite, fgcolors and bgcolors must all be equal to width*height.
 * @note The colors are given in hexadecimal and are maped to the 16 colors. Thus a color only needs one character. Non hexadecimal characters are interpreted as '0'.
 * @note See the Colors section in gamejam.h.
 * @note You might want to use sprite_editor to generate the code for your sprites.
 *
 * @return whether the sprite was successfully set
 * @retval 1 Success.
 * @retval 0 An error occured. An error message has been printed to stderr. (note: draw_destroy() clears the screen, so you might not see the message if you call draw_destroy() on failure!)
 */
int draw_sprite_set(int sid, int width, int height, const char * sprite, const char * fgcolors, const char * bgcolors, char transparent) {
	//printf("Adding sprite\n");
	if (sid > draw_internal_data.spritecount) {
		fprintf(stderr, "ERROR: draw_sprite_set: sid: %d sid is bigger that spritecount: %d\n", sid, draw_internal_data.spritecount);
		return 0;
	}
	struct draw_internal_sprite * cur = &(draw_internal_data.sprites[sid]);
	if (sprite == NULL || fgcolors == NULL || bgcolors == NULL) {
		//fprintf(stderr, "ERROR: draw_sprite_set: sid: %d at least one pointer is NULL \n", sid);
		cur->sprite = NULL;
		cur->fgcolors = NULL;
		cur->bgcolors = NULL;
		cur->width = width;
		cur->height = height;
		cur->transparent = transparent;
		return 0;
	}
	free(cur->sprite);
	free(cur->fgcolors);
	free(cur->bgcolors);
	cur->sprite = NULL;
	cur->fgcolors = NULL;
	cur->bgcolors = NULL;
	cur->width = width;
	cur->height = height;
	cur->transparent = transparent;
	size_t size = width * height;
	//printf("Testing size\n");
	if ((strlen(sprite) != size) || (strlen(fgcolors) != size) || (strlen(bgcolors) != size)) {
		fprintf(stderr, "ERROR: draw_sprite_set: sid: %d size mismatch! size: %ld strlen(sprite): %ld strlen(fgcolors): %ld strlen(bgcolors): %ld\n", sid, size, strlen(sprite), strlen(fgcolors), strlen(bgcolors));
		return 0;
	}
	
	//printf("mallocing...\n");
	if ((cur->sprite = malloc(sizeof(char) * (size+1))) == NULL) {
		fprintf(stderr, "ERROR: draw_sprite_set: sid: %d malloc failed! size: %ld \n", sid, size);
		return 0;
	}
	if ((cur->fgcolors = malloc(sizeof(int) * (size))) == NULL) {
		fprintf(stderr, "ERROR: draw_sprite_set: sid: %d malloc failed! size: %ld \n", sid, size);
		free(cur->sprite);
		return 0;
	}
	if ((cur->bgcolors = malloc(sizeof(int) * (size))) == NULL) {
		fprintf(stderr, "ERROR: draw_sprite_set: sid: %d malloc failed! size: %ld \n", sid, size);
		free(cur->sprite);
		free(cur->fgcolors);
		return 0;
	}
	
	strcpy(cur->sprite, sprite);
	
	size_t i;
	int col;
	int c;
	for (i = 0; i < size; i++) {
		col = 0;
		c = fgcolors[i];
		if (c >= '0' && c <= '9') {
			col = c - '0';
		} else {
			switch (tolower(c)) {
				case 'a': col = 10 ; break;
				case 'b': col = 11 ; break;
				case 'c': col = 12 ; break;
				case 'd': col = 13 ; break;
				case 'e': col = 14 ; break;
				case 'f': col = 15 ; break;
			}
		}
		col = (col) + (((col) < 8) ? 30 : 82);
		cur->fgcolors[i] = col;
	}
	for (i = 0; i < size; i++) {
		col = 0;
		c = bgcolors[i];
		if (c >= '0' && c <= '9') {
			col = c - '0';
		} else {
			switch (tolower(c)) {
				case 'a': col = 10 ; break;
				case 'b': col = 11 ; break;
				case 'c': col = 12 ; break;
				case 'd': col = 13 ; break;
				case 'e': col = 14 ; break;
				case 'f': col = 15 ; break;
			}
		}
		col = (col) + (((col) < 8) ? 40 : 92);
		cur->bgcolors[i] = col;
	}


	return 1;
}

/**
 * @brief Draws a sprite.
 *
 * @param[in] x   the x-coordinate
 * @param[in] y   the y-coordinate
 * @param[in] sid the sprite id
 *
 */
void draw_sprite(int x, int y, int sid) {
	struct draw_internal_sprite * cur = &(draw_internal_data.sprites[sid]);
	int i;
	int j;
	int k = 0;
	char c;
	for (i = y; i < y+cur->height; i++) {
		draw_cursor_move(x,i);
		for (j = 0; j < cur->width; j++) {
			//printf("sid: %3d k: %3d\n", sid, k);
			if ( (c = cur->sprite[k]) != cur->transparent)
				printf("\033[%dm\033[%dm%c", cur->fgcolors[k], cur->bgcolors[k], c);
			else
				draw_cursor_move(x+j+1,i);
			k++;
		}
		
	}
}

/**
 * @brief Draws a sprite only if (partially) inside a given box.
 *
 * @note The coordinates are still terminal relative (the same as in draw_sprite()).
 * @note This function is slower than draw_sprite()! Not much, but if you are drawing many sprites a seconds you might want to use draw_sprite() instead.
 *
 * @param[in] x          the x-coordinate
 * @param[in] y          the y-coordinate
 * @param[in] sid        the sprite id
 * @param[in] box_x      the x-coordinate of the box
 * @param[in] box_y      the y-coordinate of the box
 * @param[in] box_width  the width of the box
 * @param[in] box_height the height of the box
 *
 */
void draw_sprite_in_box(int x, int y, int sid, int box_x, int box_y, int box_width, int box_height) {
	struct draw_internal_sprite * cur = &(draw_internal_data.sprites[sid]);
	int i;
	int j;
	int k = 0;
	char c;
	int starty, endy;
	int startx, endx;

	starty = (y > box_y) ? y : box_y;
	endy = (y+cur->height < box_y+box_height ) ? y+cur->height : box_y+box_height;
	startx = (x > box_x) ? x : box_x;
	endx = (x+cur->width < box_x+box_width ) ? x+cur->width : box_x+box_width;

	for (i = starty; i < endy; i++) {
		for (j = startx; j < endx; j++) {
			k = (i - y) * cur->width + (j - x);
			//printf("sid: %3d k: %3d\n", sid, k);
			if ( (c = cur->sprite[k]) != cur->transparent) {
				draw_cursor_move(j,i);
				printf("\033[%dm\033[%dm%c", cur->fgcolors[k], cur->bgcolors[k], c);
			}
			//k++;
		}
		
	}
}



