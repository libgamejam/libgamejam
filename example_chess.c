#include "example_chess.h"
#include "gamejam.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>
#include <termios.h>

/* ================================ global variables ================================ */
int offset_x_g, offset_y_g; // character offsets to the beginning of the board // needed for correctly drawing sprites

/* ================================ main function ================================ */
int main(int argc, char** argv) {
	struct termios origtermsettings; // original terminal settings

	if (!draw_init(CHESS_SPRITE_COUNT, &origtermsettings)) // init draw environment
		return 1;

	if (!chess_init_sprites()) { // init sprites
		fprintf(stderr, "Error: Couldn't init sprites!\n");
		draw_destroy(&origtermsettings);
		return 1;
	}

	struct chess_game game;

	chess_init_game(&game); // init the chess game

	// first draw of the game
	draw_term_clear();
	chess_redraw_all(&game);

	input_init(); // init input environment

	chess_start_menu(); // open start menu
	chess_redraw_all(&game);



	
	/* GAME LOOP */
	struct chess_square * cursor;     // current  cursor
	cursor = chess_get_square(&game,game.cursor_x, game.cursor_y);
	enum chess_gamestate_type gamestate = CHESS_GAMESTATE_RUNNING;
	while (gamestate == CHESS_GAMESTATE_RUNNING) {
		switch(input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER | INPUT_FLAG_BLOCK, NULL)) { // read and handle input
		case 'w':
			game.cursor_y--;
			if (game.cursor_y < 0)
				game.cursor_y = 0;
			break;
		case 's':
			game.cursor_y++;
			if (game.cursor_y > 7)
				game.cursor_y = 7;
			break;
		case 'd':
			game.cursor_x++;
			if (game.cursor_x > 7)
				game.cursor_x = 7;
			break;
		case 'a':
			game.cursor_x--;
			if (game.cursor_x < 0)
				game.cursor_x = 0;
			break;
		case 'r': // redraw
			draw_term_clear();
			chess_redraw_all(&game);
			break;
		case '?':
		case 'h': // help
			chess_start_menu();
			chess_redraw_all(&game);
			break;
		case 'q': // quit
			chess_quit_menu(&gamestate);
			chess_redraw_all(&game);
			break;

		case '\n':
		case ' ': 
		case 'e':  // select
			if (game.hand == NULL) {
				// hand is empty // selecting a new piece
				if (cursor->piece && cursor->piece->player == game.currentplayer) {
					game.hand = cursor;
					chess_unselect_all(&game);
					chess_select_legit_moves(&game, cursor->piece, 0);
					cursor->seltype = CHESS_SELECTION_SOURCE;
				}
			} else {
				// moving the piece
				if (chess_move(&game,game.hand->piece, cursor)) {
					// move was successful
					chess_promotion(&game, cursor); // only does something if promotion actually possible
					game.currentplayer = (game.currentplayer == CHESS_PLAYER_WHITE) ? CHESS_PLAYER_BLACK : CHESS_PLAYER_WHITE;
					game.moveid++;
				}

				game.hand = NULL;
				chess_unselect_all(&game);
				
				game.iswhiteincheck = chess_is_check(&game, CHESS_PLAYER_WHITE);
				game.isblackincheck = chess_is_check(&game, CHESS_PLAYER_BLACK);
				game.nummovesleftwhite = chess_nummovesleft(&game, CHESS_PLAYER_WHITE);
				game.nummovesleftblack = chess_nummovesleft(&game, CHESS_PLAYER_BLACK);

			}
			chess_redraw_all(&game); // redraw after every hand interaction
			break;
		}
		
		// test if game over
		if (game.currentplayer == CHESS_PLAYER_WHITE && game.iswhiteincheck && game.nummovesleftwhite == 0) {
			gamestate = CHESS_GAMESTATE_END_CHECKMATE_WHITE; // black won
		} else if (game.currentplayer == CHESS_PLAYER_BLACK && game.isblackincheck && game.nummovesleftblack == 0) {
			gamestate = CHESS_GAMESTATE_END_CHECKMATE_BLACK; // white won
		} else if (game.currentplayer == CHESS_PLAYER_WHITE && game.nummovesleftwhite == 0) {
			gamestate = CHESS_GAMESTATE_END_DRAW_STALEMATE;
		} else if (game.currentplayer == CHESS_PLAYER_BLACK && game.nummovesleftblack == 0) {
			gamestate = CHESS_GAMESTATE_END_DRAW_STALEMATE;
		}


		// draw updated squares
		if (game.hand)
			chess_draw_square(game.hand);
		chess_draw_square(cursor); // redraw old cursor
		cursor = chess_get_square(&game,game.cursor_x, game.cursor_y); // update cursor
		chess_draw_square(cursor); // draw new cursor
		chess_draw_cursor(&game);
		draw_flush();
	}




	/* PRINT GAME OVER */
	const int optioncount = 4;
	const char * options[optioncount];
	options[0] = "";
	options[1] = "";
	options[2] = "";
	options[3] = "";
	switch (gamestate) {
	case CHESS_GAMESTATE_END_CHECKMATE_BLACK  : // white won
		options[1] = "Checkmate!";
		options[2] = "=> White won!";
		break;
	case CHESS_GAMESTATE_END_RESIGNATION_BLACK: // white won
		options[1] = "Black resigned!";
		options[2] = "=> White won!";
		break;
	case CHESS_GAMESTATE_END_CHECKMATE_WHITE  : // black won
		options[1] = "Checkmate!";
		options[2] = "=> Black won!";
		break;
	case CHESS_GAMESTATE_END_RESIGNATION_WHITE: // black won
		options[1] = "White resigned!";
		options[2] = "=> Black won!";
		break;
	case CHESS_GAMESTATE_END_DRAW_BY_AGREEMENT:
		options[2] = "=> Draw by agreement!";
		break;
	case CHESS_GAMESTATE_END_DRAW_STALEMATE   :
		options[2] = "=> Draw via stalemate!";
		break;
	default:
		options[2] = "Warn: Unknown or unhandled gamestate!";
		break;
	}
	draw_prompt_selection(-1, "===>>> Game Over! <<<===", "Press any key to continue.", optioncount, options, ' ', COLOR_WHITE, COLOR_MAGENTA, COLOR_BLACK, NULL);
	input_read(INPUT_FLAG_BLOCK, NULL); // wait for key press

	input_destroy(); // destroy input environment
	draw_destroy(&origtermsettings); // destory draw environment

	draw_color_reset();

	// print game over to the terminal
	printf("  ===>>> Game Over! <<< === \n");
	int i;
	for (i = 0; i < optioncount; i++)
		puts(options[i]);

	return 0;
}



/* ================================ functions ================================ */

/**
 * Get the pointer to a piece by id.
 *
 * @param[in,out] game the game where the piece is on
 * @param[in]     id   the id of the piece
 *
 * @note 0  <= id < 15 : white
 * @note       id = 15 : whiteking
 * @note 16 <= id < 32 : black
 * @note       id = 32 : blackking
 * @note Only the id ranges for the players and the ids of the kings are guranateed to be consistent.
 *
 * @return a pointer to the chess_piece
 */
struct chess_piece * chess_get_piece_by_id(struct chess_game * game, int id) {
	if (id < 15)
		return &(game->whitepieces[id]);
	else if (id == 15)
		return &(game->whiteking);
	if (id < 31)
		return &(game->blackpieces[id-16]);
	else
		return &(game->blackking);

}

/**
 * Draws the promotion menu.
 *
 * @param[in] square   the square the original pawn is standing on
 * @param[in] selected which piece is currently selected (0: QUEEN 1: KNIGHT 2: ROOK 3: BISHOP)
 *
 * @note The draw buffer will be flushed.
 */
void chess_draw_promotion_menu(struct chess_square * square, int selected) {
	int width = CHESS_SPRITE_WIDTH * 4 + 7;
	int height = CHESS_SPRITE_HEIGHT + 4;
	int x = (draw_term_get_cols()/2) - (width / 2) + 1; // center
	int y = (draw_term_get_rows()/2) - (height / 2) + 1;

	draw_shadowed_box(x,y,width,height,7,5,0,NULL);
	draw_print_center(x+1,y+1       ,width -2, "===>>> PROMOTION! <<<===", -1);
	draw_print_center(x+1,y+height-2,width -2, "Please choose your new piece.", -1);

	x += 2;
	y += 2;
	
	// set sprite ids
	int sidsquare = (square->iseven) ? CHESS_SPRITE_ID_SQUARE_WHITE : CHESS_SPRITE_ID_SQUARE_BLACK;
	int sidoffset = (square->piece->player == CHESS_PLAYER_WHITE ? CHESS_SPRITE_OFFSET_WHITE : CHESS_SPRITE_OFFSET_BLACK);
	int sidcursor = (square->piece->player == CHESS_PLAYER_WHITE ? CHESS_SPRITE_ID_CURSOR_WHITE : CHESS_SPRITE_ID_CURSOR_BLACK);
	
	// draw pieces + their background
	int i;
	for (i = 0; i < 4; i++)
		draw_sprite( x + i * (CHESS_SPRITE_WIDTH + 1) , y , sidsquare);
	draw_sprite( x + 0 * (CHESS_SPRITE_WIDTH + 1) , y , sidoffset + CHESS_QUEEN);
	draw_sprite( x + 1 * (CHESS_SPRITE_WIDTH + 1) , y , sidoffset + CHESS_KNIGHT);
	draw_sprite( x + 2 * (CHESS_SPRITE_WIDTH + 1) , y , sidoffset + CHESS_ROOK);
	draw_sprite( x + 3 * (CHESS_SPRITE_WIDTH + 1) , y , sidoffset + CHESS_BISHOP);

	draw_sprite( x + selected * (CHESS_SPRITE_WIDTH + 1) , y , sidcursor); // draw cursor
	draw_flush();
}


/**
 * Trys to promote the piece on a given square.
 *
 * This function will (1.) test whether the current square contains a piece and (2.) if said piece is allowed to promote right now.
 * If both conditions are true the function will open a menu asking the player to choose how the pawn should be promoted.
 *
 * @note Selection is controlled via the arrow keys or using WASD. Selection is confirmed via pressing Space, Enter or 'e'.
 *
 * @param[in,out] game    the game
 * @param[in,out] square  the square
 *
 * @return whether a promotion occured
 * @retval 1 a promotion was successfully performed
 * @retval 0 the square doesnt contain a piece, or the piece is not allowed to promote right now
 */
int chess_promotion(struct chess_game * game, struct chess_square * square) {
	if (game == NULL || square == NULL || square->piece == NULL)
		return 0;
	struct chess_piece * piece = square->piece;
	if (piece->type != CHESS_PAWN) // only pawn are allowed to promote
		return 0;

	// is the pawn at the correct position to be allowed to promote
	if ((piece->player == CHESS_PLAYER_WHITE && piece->y != 0) || (piece->player == CHESS_PLAYER_BLACK && piece->y != 7))
		return 0;


	int selected = 0; // QUEEN is selected by default
	enum chess_piece_type selectedtype;
	while (1) { // infinite loop until a promotion is made and the function returns
		chess_draw_promotion_menu(square, selected); // draw the (updated) promotion menu
		selectedtype = CHESS_QUEEN;
		switch (selected) {
		case 0: selectedtype = CHESS_QUEEN;  break;
		case 1: selectedtype = CHESS_KNIGHT; break;
		case 2: selectedtype = CHESS_ROOK;   break;
		case 3: selectedtype = CHESS_BISHOP; break;
		default: break; // should never happen
		}
		switch(input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER | INPUT_FLAG_BLOCK, NULL)) { // read and handle input
		case 'd':
		case 's':
			selected++;
			if (selected > 3)
				selected = 3;
			break;
		case 'a':
		case 'w':
			selected--;
			if (selected < 0)
				selected = 0;
			break;
		case '\n':
		case ' ':
		case 'e': // selected

			// promote the pawn
			piece->type = selectedtype;
			piece->sid = piece->type + (piece->player == CHESS_PLAYER_WHITE ? CHESS_SPRITE_OFFSET_WHITE : CHESS_SPRITE_OFFSET_BLACK);

			return 1; // successfull promotion
		}
	}
	
}
/**
 * Opens the start menu, with a welcome message and shows the basic controls.
 *
 * @note This will halt until the user pressed a key.
 */
void chess_start_menu() {
	const int optioncount = 12;
	const char * options[optioncount];
	options[ 0] = "Welcome to this libgamejam example implementation";
	options[ 1] = "of chess in C!";
	options[ 2] = "";
	options[ 3] = "Controls:";
	options[ 4] = "- WASD or arrow keys : move the cursor";
	options[ 5] = "-  Enter, Space, 'e' : (un-)select a piece";
	options[ 6] = "-                'q' : open the quit menu";
	options[ 7] = "-                'r' : redraw everything";
	options[ 8] = "-           '?', 'h' : open this help message";
	options[ 9] = "";
	options[10] = "ASCII art is based on works by Alefith 22.02.95.";
	options[11] = "<https://www.asciiart.eu/sports-and-outdoors/chess>";

	draw_prompt_selection(-1, "===>>> Chess by Jake <<<===", "Press any key to continue.", optioncount, options, ' ', COLOR_WHITE, COLOR_MAGENTA, COLOR_BLACK, NULL);
	input_read(INPUT_FLAG_BLOCK, NULL); // wait for key press
}

/**
 * Opens the quit menu.
 *
 * The players can choose whether they want to call it a draw, resign or just continue playing.
 *
 * @param[in,out] gamestate a pointer to the actual gamestate
 *
 * @note Selection is controlled via the arrow keys or using WASD. Selection is confirmed via pressing Space, Enter or 'e'.
 * @note Alternativly one can direcly press a number to choose and confirm a selection.
 * @note Pressing 'q' is equivalent to pressing '4'.
 *
 */
void chess_quit_menu(enum chess_gamestate_type * gamestate) {
	const int optioncount = 4;
	const char * options[optioncount];
	options[0] = "1. Draw by agreement.";
	options[1] = "2. Resign. (White)";
	options[2] = "3. Resign. (Black)";
	options[3] = "4. Go back.";
	
	int selected = 3; // 4. Go back. is selected by default
	while (1) { // infinite loop until a decision is made
		// draw the menu
		draw_prompt_selection(selected, "===>>> Quit Menu <<<===", "Select an action.", optioncount, options, '>', COLOR_WHITE, COLOR_MAGENTA, COLOR_BLACK, NULL);

		// update gamestate based on selection
		switch (selected) {
		case 0: *gamestate = CHESS_GAMESTATE_END_DRAW_BY_AGREEMENT; break;
		case 1: *gamestate = CHESS_GAMESTATE_END_RESIGNATION_WHITE; break;
		case 2: *gamestate = CHESS_GAMESTATE_END_RESIGNATION_BLACK; break;
		case 3: *gamestate = CHESS_GAMESTATE_RUNNING; break;
		default: break; // should never happen
		}

		switch(input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER | INPUT_FLAG_BLOCK, NULL)) { // read and handle input
		case 'd':
		case 's':
			selected++;
			if (selected > optioncount -1)
				selected = optioncount -1;
			break;
		case 'a':
		case 'w':
			selected--;
			if (selected < 0)
				selected = 0;
			break;
		case '\n':
		case ' ':
		case 'e': // confirm selection
			return;
		case '1': *gamestate = CHESS_GAMESTATE_END_DRAW_BY_AGREEMENT; return;
		case '2': *gamestate = CHESS_GAMESTATE_END_RESIGNATION_WHITE; return;
		case '3': *gamestate = CHESS_GAMESTATE_END_RESIGNATION_BLACK; return;
		case 'q':
		case '4': *gamestate = CHESS_GAMESTATE_RUNNING;               return;
		}
	}

}


/**
 * Copies the data of a single piece to another.
 *
 * @param[out] dst the destination piece
 * @param[in]  src the source piece
 *
 * @note This function is only meant to be used in the context of copying a game. See chess_copy_game
 */
void chess_copy_game_piece(struct chess_piece * dst, struct chess_piece * src) {
	dst->sid       = src->sid      ;
	dst->type      = src->type     ;
	dst->player    = src->player   ;
	dst->lastmoved = src->lastmoved;
	dst->isalive   = src->isalive  ;
	dst->x         = src->x        ;
	dst->y         = src->y        ;
}

/**
 * Finds and returns the pointer of a piece at a specific position in a game.
 *
 * @param[in] game the game
 * @param[in] x the x-coordinate on the board
 * @param[in] y the y-coordinate on the board
 *
 * @note This function is only meant to be used in the context of copying a game. See chess_copy_game
 *
 * @return a pointer to the piece
 * @retval NULL the piece wasn't found / there is no piece at that location
 */
struct chess_piece * chess_copy_game_find_piece(struct chess_game * game, int x, int y) {
	struct chess_piece * cur;
	int i;
	for (i = 0; i< 32; i++) {
		cur = chess_get_piece_by_id(game, i);
		if (cur->x == x && cur->y == y && cur->isalive) return cur;
	}
	return NULL;
}

/**
 * Copies the data of a single square to another.
 *
 * @param[out]    dst     the destination square
 * @param[in]     src     the source square
 * @param[in,out] dstgame the game that contains the destination square
 *
 * @note This function is only meant to be used in the context of copying a game. See chess_copy_game
 */
void chess_copy_game_square(struct chess_square * dst, struct chess_square * src, struct chess_game * dstgame) {
	dst->seltype = src->seltype;
	dst->iseven = src->iseven;
	dst->x = src->x;
	dst->y = src->y;
	dst->piece = chess_copy_game_find_piece(dstgame, dst->x, dst->y);
}

/**
 * Copies a game.
 *
 * @param[out] dst  the destination game
 * @param[in]  src  the source game
 *
 * @note This might be used for testing whether moves are valid by performing the move on a copy of the original game.
 *
 */
void chess_copy_game(struct chess_game * dst, struct chess_game * src) {
	int i;
	for (i = 0; i< 32; i++) {
		chess_copy_game_piece(chess_get_piece_by_id(dst, i),chess_get_piece_by_id(src, i));
	}
	dst->cursor_x = src->cursor_x;
	dst->cursor_y = src->cursor_y;
	for (i = 0; i< 64; i++) {
		chess_copy_game_square(&(dst->board[i]), &(src->board[i]), dst);
	}
	if (src->hand)
		dst->hand = chess_get_square(dst, src->hand->x, src->hand->y);
	else
		dst->hand = NULL;
	dst->moveid = src->moveid;
	dst->currentplayer = src->currentplayer;
	dst->waslastmovepawntwosquare = src->waslastmovepawntwosquare;
	dst->iswhiteincheck = src->iswhiteincheck;
	dst->isblackincheck = src->isblackincheck;
	dst->nummovesleftwhite = src->nummovesleftwhite;
	dst->nummovesleftblack = src->nummovesleftblack;
}

/**
 * Gets a pointer to the square at a given position.
 *
 * @param[in] game the game
 * @param[in] x the x-coordinate on the board
 * @param[in] y the y-coordinate on the board
 *
 * @return the pointer to square at x,y
 * @retval NULL position is outside the board
 */
struct chess_square * chess_get_square(struct chess_game * game, int x, int y) {
	if (x >= 0 && y >= 0 && x <= 7 && y <= 7)
		return &(game->board[x*8 + y]);
	return NULL;
}

/**
 * Draws a square.
 *
 * @param[in] cur the square
 *
 * @note The background of the square is drawn.
 * @note If the square has a piece, that piece is drawn too.
 * @note If the square is selected, the correct selection sprite is drawn on top.
 *
 */
void chess_draw_square(struct chess_square * cur) {
	if (cur->iseven)
		chess_draw_sprite(cur->x, cur->y, CHESS_SPRITE_ID_SQUARE_WHITE);
	else
		chess_draw_sprite(cur->x, cur->y, CHESS_SPRITE_ID_SQUARE_BLACK);

	// draw piece
	if (cur->piece != NULL && cur->piece->isalive)
		chess_draw_sprite(cur->piece->x, cur->piece->y, cur->piece->sid);

	switch (cur->seltype) {
		case CHESS_SELECTION_NONE:
			break;
		case CHESS_SELECTION_SOURCE:
			chess_draw_sprite(cur->x, cur->y, CHESS_SPRITE_ID_SELECTED_SOURCE);
			break;
		case CHESS_SELECTION_TARGET_CASTLING_KINGSIDE:
		case CHESS_SELECTION_TARGET_CASTLING_QUEENSIDE:
		case CHESS_SELECTION_TARGET_NORMAL:
			chess_draw_sprite(cur->x, cur->y, CHESS_SPRITE_ID_SELECTED_TARGET);
			break;
		case CHESS_SELECTION_TARGET_ENPASSANT:
		case CHESS_SELECTION_TARGET_ENEMY:
			chess_draw_sprite(cur->x, cur->y, CHESS_SPRITE_ID_SELECTED_TARGET_ENEMY);
			break;
	}
}

/**
 * Draws the cursor of the game.
 *
 * @param[in] game the game
 *
 * @note The cursor depends on the current player.
 */
void chess_draw_cursor(struct chess_game * game) {
	if (game->currentplayer == CHESS_PLAYER_WHITE)
		chess_draw_sprite(game->cursor_x, game->cursor_y, CHESS_SPRITE_ID_CURSOR_WHITE);
	else
		chess_draw_sprite(game->cursor_x, game->cursor_y, CHESS_SPRITE_ID_CURSOR_BLACK);
}

/**
 * Redraws the entire current state of the game.
 *
 * @param[in] game the game to draw
 *
 * @note This function will also enforce the minimum terminal size required by this game.
 * @note This function also updates the global variables offset_x_g and offset_y_g.
 * @note This function flushes the draw buffer.
 *
 */
void chess_redraw_all(struct chess_game * game) {
	int cols, rows;
	cols = draw_term_get_cols();
	rows = draw_term_get_rows();

	draw_color_reset(); // reset colors

	// enforce minimum terminal size
	while (cols < CHESS_TERM_MIN_COLS || rows < CHESS_TERM_MIN_ROWS) {
		printf("Your terminal is too small. Please enlarge it or make your font smaller.\r\n");
		printf("Current size: %dx%d\r\n", cols,rows);
		printf("Minimum needed: %dx%d\r\n", CHESS_TERM_MIN_COLS,CHESS_TERM_MIN_ROWS);
		cols = draw_term_get_cols();
		rows = draw_term_get_rows();
		usleep(10000);
		draw_term_clear();
	}

	offset_x_g = (cols/2) - (CHESS_SPRITE_WIDTH * 4) + 1; // center board // update global character drawing offsets
	offset_y_g = (rows/2) - (CHESS_SPRITE_HEIGHT * 4) + 1;

	//draw_term_clear(); // commented out as this causes flickers and it is unnecessary in most cases


	int i;
	// draw entire board
	for (i = 0; i < 64; i++) {
		chess_draw_square(&(game->board[i]));
		draw_flush();
	}

	chess_draw_cursor(game); // draw cursor


	// draw the outline around the board
	if (game->currentplayer == CHESS_PLAYER_WHITE) { // set color based on current player
		draw_color_bg(COLOR_WHITE);
		draw_color_fg(COLOR_BRIGHT_WHITE);
	} else {
		draw_color_bg(COLOR_BRIGHT_BLACK);
		draw_color_fg(COLOR_BLACK);
	}
	draw_rect_outline(offset_x_g-1,offset_y_g-1,CHESS_SPRITE_WIDTH * 8 + 2, CHESS_SPRITE_HEIGHT * 8 + 2, NULL);


	draw_flush();
}

/**
 * Perform a move on the board.
 *
 * @note This function requires that valid moves have already been selected using chess_select_legit_moves.
 *
 * @param[in,out] game  the game
 * @param[in,out] piece the piece to be moved
 * @param[in,out] dest  the destination square for the piece
 *
 * @return whether the move was successful
 * @retval 1 the move was successfully performed
 * @retval 0 impossible,illegal,invalid move
 */
int chess_move(struct chess_game * game, struct chess_piece * piece, struct chess_square * dest) {
	if (piece == NULL || dest == NULL)
		return 0;

	struct chess_square * target; // used in special cases
	struct chess_square * oldrook; // used by castling
	int mult;

	// behaviour is based on the selection_type of the destination square
	switch (dest->seltype) { 
		case CHESS_SELECTION_TARGET_NORMAL:
		case CHESS_SELECTION_TARGET_ENEMY:
			if (dest->piece != NULL) {
				if (dest->piece->player == piece->player)
					return 0;
				dest->piece->isalive = 0;
			}
			break;
		case CHESS_SELECTION_TARGET_ENPASSANT: // special case: en passant
			mult = (piece->player == CHESS_PLAYER_WHITE) ? CHESS_MOVE_DIRECTION_WHITE : CHESS_MOVE_DIRECTION_BLACK;
			target = chess_get_square(game, dest->x, dest->y - mult);
			if (dest->piece != NULL || target == NULL || target->piece == NULL || target->piece->player == piece->player) {
				return 0;
			}
			target->piece->isalive = 0;
			target->piece = NULL;
			break;
		case CHESS_SELECTION_TARGET_CASTLING_QUEENSIDE: // special case: castling queenside
			// move the rook
			oldrook = chess_get_square(game, 0, dest->y);
			target = chess_get_square(game, 3, dest->y);
			target->piece = oldrook->piece;
			oldrook->piece = NULL;
			target->piece->x = target->x;
			target->piece->y = target->y;
			break;
		case CHESS_SELECTION_TARGET_CASTLING_KINGSIDE: // special case: castling kingside
			// move the rook
			oldrook = chess_get_square(game, 7, dest->y);
			target = chess_get_square(game, 5, dest->y);
			target->piece = oldrook->piece;
			oldrook->piece = NULL;
			target->piece->x = target->x;
			target->piece->y = target->y;
			break;
		default:
			return 0; // destination is not selected and thus not a valid move destination for this piece
	}
	
	// update whether this move was a pawn moving two squares
	game->waslastmovepawntwosquare = (piece->type == CHESS_PAWN && piece->lastmoved == -1 && abs(piece->y - dest->y) > 1);

	// move the actual piece
	struct chess_square * oldsquare = chess_get_square(game,piece->x, piece->y);
	piece->lastmoved = game->moveid;
	dest->piece = piece;
	oldsquare->piece = NULL;
	piece->x = dest->x;
	piece->y = dest->y;
	return 1; // successfully moved
}

/**
 * Unselect all squares.
 *
 * Sets seltype for every square to CHESS_SELECTION_NONE.
 *
 * @param[in,out] game the game
 *
 *
 * @note See chess_select_legit_moves
 */
void chess_unselect_all(struct chess_game * game) {
	int i,j;
	struct chess_square * cur;
	for (i = 0; i< 8; i++) {
		for (j = 0; j< 8; j++) {
			cur = chess_get_square(game,i,j);
			cur->seltype = CHESS_SELECTION_NONE;
		}
	}


}

/**
 * Trys to select a square for a valid move if said move is valid.
 *
 * @param[in,out] game               the game
 * @param[in]     piece              the piece to pretend move
 * @param[in,out] square             the pretend move desitination
 * @param[out]    state              whether a piece was at the destination
 * @param[in]     skipchecktest      if ==1 skip the test for whether this move results in a check for the own team
 * @param[in]     emptysquareseltype the selection type the square should be assigned to if the square is empty (mostly used for tracking special cases)
 *
 * @note If either piece or square are NULL, the function does nothing and returns 0.
 * @note emptysquareseltype should be CHESS_SELECTION_TARGET_NORMAL for non-special cases.
 *
 * @return whether the square was selected
 * @retval 1 the square is selected
 * @retval 0 the square is not selected
 */
int chess_try_select_move(struct chess_game * game, struct chess_piece * piece, struct chess_square * square, int * state, int skipchecktest, enum chess_selection_type emptysquareseltype) {
	if (piece == NULL || square == NULL)
		return 0;
	*state = (square->piece != NULL); // update state

	// test whether the square contains a piece owned by the same player as this piece
	if (square->piece != NULL && square->piece->player == piece->player)
		return 0;

	if (square->piece != NULL)
		square->seltype = CHESS_SELECTION_TARGET_ENEMY;
	else
		square->seltype = emptysquareseltype;

	if (skipchecktest)
		return 1;
	
	// test whether the move will result in a check for the own king
	static struct chess_game gamecopy; // static
	chess_copy_game(&gamecopy, game);
	chess_move(&gamecopy, chess_copy_game_find_piece(&gamecopy, piece->x, piece->y) , chess_get_square(&gamecopy, square->x, square->y));
	if (chess_is_check(&gamecopy, piece->player)) {
		square->seltype = CHESS_SELECTION_NONE;
		return 0;
	}
	
	return 1;
}

/**
 * Selects all legit moves that a piece can make.
 *
 * @note This function contains most of the valid movement logic for the pieces.
 *
 * @param[in,out] game          the game
 * @param[in,out] piece         the piece
 * @param[in]     skipchecktest if ==1 skip testing for whether a move results in a check for the own team
 *
 * @return number of legit moves found/selected
 * @retval 0 no legit moves found or the piece is dead
 *
 */
int chess_select_legit_moves(struct chess_game * game, struct chess_piece * piece, int skipchecktest) {
	if (piece == NULL || piece->isalive == 0)
		return 0;
	int res = 0; // result
	int i;
	int state = 0;
	int x = piece->x;
	int y = piece->y;
	struct chess_square * tmp;
	int mult; // a multiplier for the movement direction of pawns
	switch (piece->type) {
		case (CHESS_PAWN):
			mult = (piece->player == CHESS_PLAYER_WHITE) ? CHESS_MOVE_DIRECTION_WHITE : CHESS_MOVE_DIRECTION_BLACK;
 			if ((tmp = chess_get_square(game,x,y+mult)) != NULL && tmp->piece == NULL) {
				if (chess_try_select_move(game,piece, tmp, &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
				if (piece->lastmoved == -1) { // hasn't moved yet
 					if ((tmp = chess_get_square(game,x,y+(2*mult))) != NULL && tmp->piece == NULL) {
						if (chess_try_select_move(game,piece, tmp, &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
					}
				}
			}
 			if ((tmp = chess_get_square(game,x+1,y+mult)) != NULL && tmp->piece != NULL) {
				if (chess_try_select_move(game,piece, tmp, &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			}
 			if ((tmp = chess_get_square(game,x-1,y+mult)) != NULL && tmp->piece != NULL) {
				if (chess_try_select_move(game,piece, tmp, &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			}

			// en passant
 			if (game->waslastmovepawntwosquare && (tmp = chess_get_square(game,x+1,y)) != NULL && tmp->piece != NULL && tmp->piece->lastmoved == game->moveid - 1) {
				if (chess_try_select_move(game,piece, chess_get_square(game,x+1,y+mult), &state,skipchecktest,CHESS_SELECTION_TARGET_ENPASSANT)) res++;
			}
 			if (game->waslastmovepawntwosquare && (tmp = chess_get_square(game,x-1,y)) != NULL && tmp->piece != NULL && tmp->piece->lastmoved == game->moveid - 1) {
				if (chess_try_select_move(game,piece, chess_get_square(game,x-1,y+mult), &state,skipchecktest,CHESS_SELECTION_TARGET_ENPASSANT)) res++;
			}
			break;
		case (CHESS_ROOK):
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x+i,y), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x-i,y), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x,y+i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x,y-i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			break;
		case (CHESS_KNIGHT):
			if (chess_try_select_move(game,piece, chess_get_square(game,x+1,y+2), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x+1,y-2), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x-1,y+2), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x-1,y-2), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;

			if (chess_try_select_move(game,piece, chess_get_square(game,x+2,y+1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x-2,y+1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x+2,y-1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x-2,y-1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			break;
		case (CHESS_BISHOP):
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x+i,y+i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x-i,y+i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x+i,y-i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x-i,y-i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			break;
		case (CHESS_QUEEN):
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x+i,y+i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x-i,y+i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x+i,y-i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x-i,y-i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x+i,y), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x-i,y), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x,y+i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			state = 0; for (i = 1; i < 8 && state == 0; i++) if (chess_try_select_move(game,piece, chess_get_square(game,x,y-i), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			break;
		case (CHESS_KING):
			if (chess_try_select_move(game,piece, chess_get_square(game,x+1,y-1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x+1,y+1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x  ,y-1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x  ,y+1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x-1,y-1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;
			if (chess_try_select_move(game,piece, chess_get_square(game,x-1,y+1), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) res++;

			int isincheck = (piece->player == CHESS_PLAYER_WHITE) ? game->iswhiteincheck : game->isblackincheck;

			state = 0; if (chess_try_select_move(game,piece, chess_get_square(game,x+1,y), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) {
				res++;
				// castling kingside
				if (state == 0 && piece->lastmoved == -1 && !isincheck && (tmp = chess_get_square(game,7,y)) != NULL && tmp->piece != NULL && tmp->piece->type == CHESS_ROOK && tmp->piece->lastmoved == -1 && tmp->piece->isalive) {
					if ((tmp = chess_get_square(game,x+2,y))->piece == NULL && chess_try_select_move(game,piece, tmp, &state,skipchecktest,CHESS_SELECTION_TARGET_CASTLING_KINGSIDE)) res++;
				}
			}

			state = 0; if (chess_try_select_move(game,piece, chess_get_square(game,x-1,y), &state,skipchecktest,CHESS_SELECTION_TARGET_NORMAL)) {
				res++;
				// castling queenside
				if (state == 0 && piece->lastmoved == -1 && !isincheck && (tmp = chess_get_square(game,0,y)) != NULL && tmp->piece != NULL && tmp->piece->type == CHESS_ROOK && tmp->piece->lastmoved == -1 && tmp->piece->isalive) {
					if ((tmp = chess_get_square(game,1,y))->piece == NULL) {
						if ((tmp = chess_get_square(game,x-2,y))->piece == NULL && chess_try_select_move(game,piece, tmp, &state,skipchecktest,CHESS_SELECTION_TARGET_CASTLING_QUEENSIDE)) res++;
					}
				}
			}

			break;
	}

	return res;
}

/**
 * Calculates the total number of legit moves the given player can make right now.
 *
 * @note These moves are tested and calculated on a copy of the game.
 *
 * @param[in] game   the game
 * @param[in] player the player
 *
 * @return number of legit moves the player can make right now
 */
int chess_nummovesleft(struct chess_game * game, enum chess_player_type player) {
	static struct chess_game gamecopy; // static
	
	chess_copy_game(&gamecopy, game); // copy the game
	struct chess_piece * cur; // current piece
	int playeroffset = (player == CHESS_PLAYER_WHITE) ? 0 : 16; // 0-15: white // 16-32: black // see chess_get_piece_by_id
	int i;
	int res = 0;
	for (i = playeroffset; i< playeroffset+16; i++) {
		chess_unselect_all(&gamecopy);
		cur = chess_get_piece_by_id(&gamecopy, i);
		res += chess_select_legit_moves(&gamecopy, cur, 0);
	}
	return res; // result
}

/**
 * Gets whether the given player is currently under check by the enemy.
 *
 * @note This will call chess_unselect_all at the end.
 *
 * @param[in,out] game   the game
 * @param[in    ] player the player
 *
 * @return whether the king is under check
 */
int chess_is_check(struct chess_game * game, enum chess_player_type player) {
	struct chess_piece * king = (player == CHESS_PLAYER_WHITE) ? (&game->whiteking) : (&game->blackking);
	struct chess_piece * cur;
	int playeroffset = (player != CHESS_PLAYER_WHITE) ? 0 : 16; // set offset to ENEMY pieces // see chess_get_piece_by_id
	chess_unselect_all(game);
	int i;
	// select all legit moves the enemy can do
	for (i = playeroffset; i< playeroffset+16; i++) { // loop over ENEMY pieces
		cur = chess_get_piece_by_id(game, i);
		chess_select_legit_moves(game, cur, 1);
	}
	enum chess_selection_type res = chess_get_square(game,king->x,king->y)->seltype;
	chess_unselect_all(game);
	return (res == CHESS_SELECTION_TARGET_ENEMY); // return whether the king is selected and thus under attack
}

/**
 * Initializes a single piece.
 *
 * @note Also updates the square the piece is standing on.
 *
 * @param[in,out] game   the game
 * @param[in,out] cur    the piece
 * @param[in]     type   the type of the piece
 * @param[in]     player the owner
 * @param[in]     x      the x-coordinate of the piece on the board
 * @param[in]     y      the y-coordinate of the piece on the board
 */
void chess_init_piece(struct chess_game * game, struct chess_piece * cur, enum chess_piece_type type, enum chess_player_type player, int x, int y) {
	cur->type = type;
	cur->player = player;
	cur->x = x;
	cur->y = y;
	cur->lastmoved = -1;
	cur->isalive = 1;
	// set the sprite id needed for drawing
	cur->sid = type + (player == CHESS_PLAYER_WHITE ? CHESS_SPRITE_OFFSET_WHITE : CHESS_SPRITE_OFFSET_BLACK);
	// tell the square the piece is standing on, that it has a piece now on it
	chess_get_square(game,x,y)->piece = cur;
}

/**
 * Initializes the game and board.
 *
 * @note Sets the default cursorpositon to x=4 y=6.
 *
 * @param[in,out] game the game
 *
 */
void chess_init_game(struct chess_game * game) {
	game->cursor_x = 4;
	game->cursor_y = 6;
	game->currentplayer = CHESS_PLAYER_WHITE;
	game->hand = NULL;
	game->moveid = 0;
	game->waslastmovepawntwosquare = 0;
	game->iswhiteincheck = 0;
	game->isblackincheck = 0;
	int i,j, k = 0;
	struct chess_square * cur;
	for (i = 0; i< 8; i++) {
		k = i;
		for (j = 0; j< 8; j++) {
			cur = chess_get_square(game,i,j);
			cur->seltype = CHESS_SELECTION_NONE;
			cur->piece = NULL;
			cur->x = i;
			cur->y = j;
			cur->iseven = !(k % 2);
			k++;
		}
	}

	int id = 0;
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_ROOK,   CHESS_PLAYER_WHITE, 0, 7);
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_ROOK,   CHESS_PLAYER_WHITE, 7, 7);
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_KNIGHT, CHESS_PLAYER_WHITE, 1, 7);
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_KNIGHT, CHESS_PLAYER_WHITE, 6, 7);
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_BISHOP, CHESS_PLAYER_WHITE, 2, 7);
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_BISHOP, CHESS_PLAYER_WHITE, 5, 7);
	chess_init_piece(game,&(game->whitepieces[id++]), CHESS_QUEEN,  CHESS_PLAYER_WHITE, 3, 7);
	chess_init_piece(game,&(game->whiteking), CHESS_KING,   CHESS_PLAYER_WHITE, 4, 7);
	for (i = 0; i < 8; i++)
		chess_init_piece(game,&(game->whitepieces[id++]), CHESS_PAWN,   CHESS_PLAYER_WHITE, i, 6);

	id = 0;
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_ROOK,   CHESS_PLAYER_BLACK, 0, 0);
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_ROOK,   CHESS_PLAYER_BLACK, 7, 0);
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_KNIGHT, CHESS_PLAYER_BLACK, 1, 0);
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_KNIGHT, CHESS_PLAYER_BLACK, 6, 0);
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_BISHOP, CHESS_PLAYER_BLACK, 2, 0);
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_BISHOP, CHESS_PLAYER_BLACK, 5, 0);
	chess_init_piece(game,&(game->blackpieces[id++]), CHESS_QUEEN,  CHESS_PLAYER_BLACK, 3, 0);
	chess_init_piece(game,&(game->blackking), CHESS_KING,   CHESS_PLAYER_BLACK, 4, 0);
	for (i = 0; i < 8; i++)
		chess_init_piece(game,&(game->blackpieces[id++]), CHESS_PAWN,   CHESS_PLAYER_BLACK, i, 1);

	game->nummovesleftwhite = chess_nummovesleft(game, CHESS_PLAYER_WHITE);
	game->nummovesleftblack = chess_nummovesleft(game, CHESS_PLAYER_BLACK);

}

/**
 * Initializes all sprites.
 *
 * @return whether it was successful
 * @retval 0 An error occured. Sprites might not be initialized.
 * @retval 1 Success.
 *
 */
int chess_init_sprites() {
	//printf("DEBUG: initsprites!\n");
	if (!draw_sprite_set(CHESS_SPRITE_ID_SQUARE_BLACK, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
                \
                \
                \
                \
                \
                \
                \
                ","\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888","\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888\
8888888888888888", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_ID_SQUARE_WHITE, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
                \
                \
                \
                \
                \
                \
                \
                ","\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777","\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777\
7777777777777777", '.')) return 0;

// black pieces
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_PAWN, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
................\
................\
......._........\
......(_).......\
.....(___)......\
....._|_|_......\
....(_____).....\
..../__P__\\.....","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_ROOK, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
...._.._.._.....\
...| || || |....\
...|_______|....\
...\\__ ___ /....\
....|_|___|.....\
....|___|_|.....\
...(_______)....\
.../___R___\\....","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_KNIGHT, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......^^__......\
...../  - \\_....\
...<|    __<....\
...<|     \\.....\
...<|______\\....\
...._|____|_....\
...(________)...\
.../___N____\\...","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_BISHOP, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......_O........\
...../ //\\......\
.....\\___/......\
.....(___)......\
...../   \\......\
....(_____).....\
...(_______)....\
.../___B___\\....","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_QUEEN, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......_()_......\
...._/____\\_....\
....\\      /....\
.....\\____/.....\
...../    \\.....\
....(______)....\
...(________)...\
.../___Q____\\...","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_BLACK + CHESS_KING, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......_TT_......\
...._/____\\_....\
....\\      /....\
.....\\____/.....\
...../    \\.....\
....(______)....\
...(________)...\
.../___K____\\...","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000", '.')) return 0;

// white pieces
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_WHITE + CHESS_PAWN, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
................\
................\
......._........\
......(_).......\
.....(___)......\
....._|_|_......\
....(_____).....\
..../__P__\\.....","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_WHITE + CHESS_ROOK, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
...._.._.._.....\
...| || || |....\
...|_______|....\
...\\__ ___ /....\
....|_|___|.....\
....|___|_|.....\
...(_______)....\
.../___R___\\....","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_WHITE + CHESS_KNIGHT, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......^^__......\
...../  - \\_....\
...<|    __<....\
...<|     \\.....\
...<|______\\....\
...._|____|_....\
...(________)...\
.../___N____\\...","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_WHITE + CHESS_BISHOP, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......_O........\
...../ //\\......\
.....\\___/......\
.....(___)......\
...../   \\......\
....(_____).....\
...(_______)....\
.../___B___\\....","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_WHITE + CHESS_QUEEN, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......_()_......\
...._/____\\_....\
....\\      /....\
.....\\____/.....\
...../    \\.....\
....(______)....\
...(________)...\
.../___Q____\\...","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_OFFSET_WHITE + CHESS_KING, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
......_TT_......\
...._/____\\_....\
....\\      /....\
.....\\____/.....\
...../    \\.....\
....(______)....\
...(________)...\
.../___K____\\...","\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000\
0000000000000000","\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF\
FFFFFFFFFFFFFFFF", '.')) return 0;







	if (!draw_sprite_set(CHESS_SPRITE_ID_SELECTED_TARGET, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
####........####\
##............##\
................\
................\
................\
................\
##............##\
####........####","\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333","\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333\
3333333333333333", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_ID_SELECTED_TARGET_ENEMY, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
####........####\
##............##\
................\
................\
................\
................\
##............##\
####........####","\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111","\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111\
1111111111111111", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_ID_CURSOR_BLACK, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
..##........##..\
####........####\
................\
................\
................\
................\
####........####\
..##........##..","\
2222222222222222\
2200222222220022\
2222222222222222\
2222222222222222\
2222222222222222\
2222222222222222\
2200222222220022\
2222222222222222","\
2222222222222222\
2200222222220022\
2222222222222222\
2222222222222222\
2222222222222222\
2222222222222222\
2200222222220022\
2222222222222222", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_ID_CURSOR_WHITE, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
..##........##..\
####........####\
................\
................\
................\
................\
####........####\
..##........##..","\
2222222222222222\
22FF22222222FF22\
2222222222222222\
2222222222222222\
2222222222222222\
2222222222222222\
22FF22222222FF22\
2222222222222222","\
2222222222222222\
22FF22222222FF22\
2222222222222222\
2222222222222222\
2222222222222222\
2222222222222222\
22FF22222222FF22\
2222222222222222", '.')) return 0;
	if (!draw_sprite_set(CHESS_SPRITE_ID_SELECTED_SOURCE, CHESS_SPRITE_WIDTH, CHESS_SPRITE_HEIGHT,"\
####........####\
##............##\
................\
................\
................\
................\
##............##\
####........####","\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555","\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555\
5555555555555555", '.')) return 0;

		return 1;
}

