#ifndef GAMEJAM_SNAKE
#define GAMEJAM_SNAKE

#define snake_draw_point(x,y,col) {draw_cursor_move(offset_x_g + (x)*2, offset_y_g + (y)); draw_color_bg((col)); draw_color_fg((col)); putchar(' '); putchar(' '); }

struct snake_element {
	int x;
	int y;
	struct snake_element * prev; // head <- tail
	struct snake_element * next; // head -> tail
};

struct snake_game {
	int width; // width and height of the game board
	int height;

	int solidwall; // 1: solidwall 0: teleport wall

	int applex; // apple coordinates // 37°20′5″N 122°0′32″W
	int appley;
	int justeatenapple; // whether the snake has just eaten an apple

	int length; // current length of the snake
	int dirx; // direction the snake is moving in
	int diry;
	struct snake_element * head; // pointer to the head of the snake
	struct snake_element * tail; // pointer to the tail of the snake

	long waittime; // wait time between ticks (microseconds)
	
};

struct snake_element * snake_new_element(int x, int y, struct snake_element * prev);

int snake_is_point_on_snake(struct snake_game * game, int x, int y);

void snake_generate_new_apple(struct snake_game * game);

void snake_init_game(struct snake_game * game);

void snake_redraw_all(struct snake_game * game);

int snake_has_eaten_self(struct snake_game * game);

void snake_set_dir(struct snake_game * game, int dirx, int diry);

void snake_pause(struct snake_game * game);

void snake_start_menu();
#endif
