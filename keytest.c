#include "gamejam.h"

int main(int argc, char ** argv) {

	struct termios origtermsettings; // original terminal settings
	if (!draw_init(16, &origtermsettings))
		return 1;
	input_init();

	draw_color_reset();
	draw_term_clear();

	printf("Press any key to get the equivalent char sequence.\r\n");
	printf("Press 'q' to quit.\r\n\r\n");

	char c;
	do {
		c = input_read(INPUT_FLAG_BLOCK, NULL);
		printf("Read char: %3d c: '%c'\r\n", (int)c, c);
		draw_flush();
	} while (c != 'q');

	input_destroy();
	draw_destroy(&origtermsettings);


	return 0;
}
