#ifndef GAMEJAM_HEADER
#define GAMEJAM_HEADER

#include <stdio.h>
#include <termios.h>
#include <unistd.h>

/**
 * @file
 * @brief Header file and public api for libgamejam.
 *
 * # The draw environment
 *
 * Most functions in this libary require to be executed inside a "draw environment".
 * 
 * You can only have one draw environment at a time!
 *
 * You can create a draw environment using draw_init().
 * And get out of a draw environment using draw_destroy().
 *
 * For an example of this see the documentation of draw_init().
 *
 * While you are inside a draw environment you can still use functions like printf() and putchar().
 * You can even change the position, text color and background color of the printed characters by calling
 * draw_cursor_move(), draw_color_fg() and draw_color_bg() before calling printf() or putchar().
 *
 * ## Weird behaviour
 *
 * While inside a draw environment the terminal behaves different than you are used to,
 * as it will heavily change the terminal settings!!!
 * (Don't worry, draw_destroy() will reset the everything to the original settings.)
 *
 * printf() might not print anything at all until you call draw_flush(). Please also read the documentation for draw_flush().
 * 
 * Line breaks ("\n") will NOT return the cursor to the beginning of the line!
 * Thus you need to use "\r\n" instead of just "\n".
 *
 *
 * # Coordinates
 *
 * The coordinate system in the terminal and in libgamejam works from the Top-Left-Corner of the terminal to the Bottom-Right-Corner.
 * 
 * The coordinates of the Top-Left-Corner are (1,1)! Coordinates in the terminal start at 1! Not 0!!!
 *
 * The coordinates of the Bottom-Right-Corner are (draw_term_get_cols(),draw_term_get_rows())!
 *
 * The x-coordinates are always refering to the columns (short: cols) of the terminal.
 * The y-coordinates are always refering to the rows of the terminal.
 *
 * Functions that take global coordinates as arguments:
 * - draw_cursor_move()
 * - draw_print_fixedwidth()
 * - draw_print_center()
 * - draw_print_tickertext()
 * - draw_rect_fill()
 * - draw_rect_outline()
 * - draw_rect_shadow()
 * - draw_shadowed_box()
 * - draw_line_vert()
 * - draw_line_hori()
 * - draw_sprite()
 * - draw_sprite_in_box()
 * 
 *
 * # Colors:
 * Some functions require a color as an argument.
 * In the following table you can see which color has which number.
 *
 * |Macro               |Dec|Hex| Color               |
 * |--------------------|---|---|---------------------|
 * |COLOR_BLACK         |0  |0  | Black               |
 * |COLOR_RED           |1  |1  | Red                 |
 * |COLOR_GREEN         |2  |2  | Green               |
 * |COLOR_YELLOW        |3  |3  | Yellow              |
 * |COLOR_BLUE          |4  |4  | Blue                |
 * |COLOR_MAGENTA       |5  |5  | Magenta             |
 * |COLOR_CYAN          |6  |6  | Cyan                |
 * |COLOR_WHITE         |7  |7  | White               |
 * |COLOR_BRIGHT_BLACK  |8  |8  | Bright Black (Gray) |
 * |COLOR_BRIGHT_RED    |9  |9  | Bright Red          |
 * |COLOR_BRIGHT_GREEN  |10 |A  | Bright Green        |
 * |COLOR_BRIGHT_YELLOW |11 |B  | Bright Yellow       |
 * |COLOR_BRIGHT_BLUE   |12 |C  | Bright Blue         |
 * |COLOR_BRIGHT_MAGENTA|13 |D  | Bright Magenta      |
 * |COLOR_BRIGHT_CYAN   |14 |E  | Bright Cyan         |
 * |COLOR_BRIGHT_WHITE  |15 |F  | Bright White        |
 *
 * Functions that take a color as an argument:
 * - draw_prompt_selection()
 * - draw_shadowed_box()
 * - draw_color_fg()
 * - draw_color_bg()
 *
 */


/* ================== draw stuff ================== */


#define COLOR_BLACK          0  
#define COLOR_RED            1  
#define COLOR_GREEN          2  
#define COLOR_YELLOW         3  
#define COLOR_BLUE           4  
#define COLOR_MAGENTA        5  
#define COLOR_CYAN           6  
#define COLOR_WHITE          7  
#define COLOR_BRIGHT_BLACK   8  
#define COLOR_BRIGHT_RED     9  
#define COLOR_BRIGHT_GREEN   10 
#define COLOR_BRIGHT_YELLOW  11 
#define COLOR_BRIGHT_BLUE    12 
#define COLOR_BRIGHT_MAGENTA 13 
#define COLOR_BRIGHT_CYAN    14 
#define COLOR_BRIGHT_WHITE   15 


/**
 * @brief Clears the terminal.
 */
#define draw_term_clear() printf("\033[0m\033[H\033[J")
/**
 * @brief Moves the cursor to x,y.
 */
#define draw_cursor_move(x,y) printf("\033[%d;%dH", (y), (x))
/**
 * @brief Hides the cursor.
 */
#define draw_cursor_hide() printf("\e[?25l")
/**
 * @brief Shows the cursor.
 */
#define draw_cursor_show() printf("\e[?25h")
/**
 * @brief Sets the text color.
 *
 * @note The color is a number. 0 <= col <= 15
 * @note See the colors section.
 */
#define draw_color_fg(col) printf("\033[%dm", (col) + (((col) < 8) ? 30 : 82))
/**
 * @brief Sets the background color.
 *
 * @note The color is a number. 0 <= col <= 15
 * @note See the colors section.
 */
#define draw_color_bg(col) printf("\033[%dm", (col) + (((col) < 8) ? 40 : 92))
/**
 * @brief Resets the text and background color.
 */
#define draw_color_reset() printf("\033[0m")
/**
 * @brief Flushes the draw buffer.
 *
 * You need to call this function regularly to clean the draw buffer. Otherwise stuff might not render correctly.
 * My tip: Always call this function after a printf() for a string or after calling draw_sprite() or draw_sprite_in_box().
 *
 * @note This function has a small delay, due to how non-blocking writing works. Thus you shouldn't call this too often or your game will slow down!
 *
 */
#define draw_flush() { fflush(stdout); usleep(250); }
//#define draw_flush() { tcflush(STDOUT_FILENO, TCIOFLUSH); }
//#define draw_flush() { tcflow(STDOUT_FILENO, TCION); }
//#define draw_flush() { fflush(stdout); tcdrain(STDOUT_FILENO); }
//#define draw_flush() { tcdrain(STDOUT_FILENO); }

/* === INTERNAL STUFF === JUST IGNORE THIS PART === */
/**
 * @brief Internal struct. Ignore this struct.
 */
struct draw_internal_sprite {
	int width;
	int height;
	char * sprite;
	int * fgcolors;
	int * bgcolors;
	char transparent;
};
/**
 * @brief Internal struct. Ignore this struct.
 */
struct draw_internal {
	int termrows;
	int termcols;
	int spritecount;
	struct draw_internal_sprite * sprites;
};
/* === END OF INTERNAL STUFF === */

/**
 * @brief Struct for describing the sides and corners of a box.
 */
struct draw_border {
	int n;  ///< north
	int e;  ///< east
	int s;  ///< south
	int w;  ///< west
	int nw; ///< northwest
	int ne; ///< northeast
	int se; ///< southeast
	int sw; ///< southwest
};


int draw_init(int spritecount, struct termios * origtermsettings);
void draw_destroy(struct termios * origtermsettings);

int  draw_term_get_cols();
int  draw_term_get_rows();
void draw_term_echo_show();
void draw_term_echo_hide();

void draw_print_fixedwidth(int x, int y, int width, const char * str);
void draw_print_center(int x, int y, int width, const char * str, int len);
void draw_print_tickertext(int x, int y, int width, int offset, const char * str, int space);

void draw_prompt_selection(int selected, const char * title, const char * subtitle, int optioncount, const char ** options, char selectionchar, int fgcolor, int bgcolor, int shadowcolor, struct draw_border * border);

void draw_rect_fill(int x, int y, int width, int height, char c);
void draw_rect_outline(int x, int y, int width, int height, struct draw_border * border);
void draw_rect_shadow(int x, int y, int width, int height, char c);
void draw_shadowed_box(int x, int y, int width, int height, int fgcolor, int bgcolor, int shadowcolor, struct draw_border * border);
void draw_line_vert(int x, int y, int height, char c);
void draw_line_hori(int x, int y, int width, char c);

int draw_sprite_set(int sid, int width, int height, const char * sprite, const char * fgcolors, const char * bgcolors, char transparent);
void draw_sprite(int x, int y, int sid);
void draw_sprite_in_box(int x, int y, int sid, int box_x, int box_y, int box_width, int box_height);



/* ================== input stuff ================== */

/**
 * @brief Bit Flag for telling input_read() that it should convert arrow keys to their WASD equivalent
 */
#define INPUT_FLAG_ARROWSTOWASD 0x1
/**
 * @brief Bit Flag for telling input_read() that it should convert its output to lower case
 */
#define INPUT_FLAG_TOLOWER 0x2
/**
 * @brief Bit Flag for telling input_read() that it should block until input was read
 */
#define INPUT_FLAG_BLOCK 0x4

void input_init();
void input_destroy();
int input_read(unsigned int flags, int *state);

#endif
