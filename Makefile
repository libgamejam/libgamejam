
TARGETS := example_chess example_snake testdraw sprite_editor keytest

# TODO update everything to a nicer standard and add -pedantic-errors
CFLAGS := -std=gnu90 -fPIC -O2 -Wall -Werror -g -I.
CC := gcc

.PHONY: default
default: lib $(TARGETS)

.PHONY: all
all: lib $(TARGETS) doc

.PHONY: clean
clean:
	$(RM) *.o *.so* *.a
	$(RM) $(TARGETS)

.PHONY: lib
lib: libgamejam.so libgamejam.a

.PHONY: doc
doc:
	doxygen Doxyfile
	make -C doc/latex/


libgamejam.so: libgamejam.a
	$(CC) -shared $^ -o $@

libgamejam.a: gamejam_input.o gamejam_draw.o gamejam.h
	ar -rc $@ $^



sprite_editor: sprite_editor.o libgamejam.a

keytest: keytest.o libgamejam.a


### EXAMPLES

example_chess: example_chess.o libgamejam.a

example_snake: example_snake.o libgamejam.a

testdraw: testdraw.o libgamejam.a




