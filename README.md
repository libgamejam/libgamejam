# libgamejam

A simple C library for drawing on the terminal (with sprite support) and reading input. Meant to be used for simple games (for example in game jams).

The library is only designed to work with ASCII-Characters (especially in sprites and borders!).

Only GNU/Linux x86_64 Systems are currently supported.
The terminal that is used while executing a program that is using this library must have support for at least 16 colors.
Support for Windows and Mac Systems is not planned.

This software was developed and tested on an Ubuntu 20.04.2 LTS 64-bit machine. Using a GNOME Terminal 3.36.2 (Using VTE version 0.60.3 +BIDI +GNUTLS +ICU +SYSTEMD). 

[[_TOC_]]

## Dependencies

You need the following tools installed for this to work:
- git
- make
- gcc
- doxygen (optional, used by documentation)
- texlive-base (optional, used by documentation)
- texlive-latex-base (optional, used by documentation)
- texlive-latex-recommended (optional, used by documentation)
- texlive-latex-extra (optional, used by documentation)

On Debian or Ubuntu systems you can easily install all of the dependencies using the following commands:
```
sudo apt-get -y install build-essential git
sudo apt-get -y install doxygen texlive-base texlive-latex-base texlive-latex-recommended texlive-latex-extra
```

## How to build
You can build the entire library (libgamejam.so and libgamejam.a) as well as the examples and the sprite editor by simply using the command `make`.

Use `make lib` if you only want to build the library.

Use `make doc` if you want to build the documentation.

Use `make all` if you want to build the library, the examples, the sprite editor and the documentation.

Use `make clean` if you want to clean everything up.

## How to use for your project
### ... when using no special environment
Note: This might be the easiest guide to follow. Especially if you have never worked with a building system(e.g. `make`). 

Just follow these exact commands (detailed explanation below):
```
mkdir yourgame
cd yourgame
git clone https://gitlab.gwdg.de/libgamejam/libgamejam.git
cd libgamejam && make && cd ..
cp libgamejam/libgamejam.a .
cp libgamejam/gamejam.h .
touch yourgame.c
```
You can now write your code in `yourgame.c`.

You can compile your code using:
```
gcc -O2 -Wall -Werror -g -o yourgame yourgame.c libgamejam.a
```

After that you can run your game using `./yourgame`.

A simple 'hello world' program might look something like this:
```c
#include "gamejam.h"
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char** argv) {
	struct termios origtermsettings; // original terminal settings are stored in here (you don't need to think too much about this, it's just needed by draw_init and draw_destroy)

	if (!draw_init(16, &origtermsettings)) // init draw environment
		return 1;
	draw_term_clear(); // clear terminal

	input_init(); // init input environment


	const int optioncount = 4;
	const char * options[optioncount];
	options[0] = "Hello World";
	options[1] = "This is my awesome game.";
	options[2] = "Currently it just shows this window but someday it will be awesome.";
	options[3] = "Yeah!";
	draw_prompt_selection(-1, "===>>> My awesome game! <<<===", "Press any key to continue.", optioncount, options, ' ', COLOR_WHITE, COLOR_BLUE, COLOR_BRIGHT_BLACK, NULL);

	input_read(INPUT_FLAG_BLOCK, NULL); // wait for key press


	input_destroy(); // destroy input environment
	draw_destroy(&origtermsettings); // destory draw environment
	return 0;
}
```

#### Explanation of commands

The commands `mkdir yourgame` and `cd yourgame` just create a directory named `yourgame/` and moves you into it.

The command `git clone https://gitlab.gwdg.de/libgamejam/libgamejam.git` will download the source code for libgamejam into the directory `libgamejam/`

The command `cd libgamejam && make && cd ..` will simply build libgamejam.

The commands `cp libgamejam/libgamejam.a .` and `cp libgamejam/gamejam.h .` will simply copy the files `libgamejam.a` and `gamejam.h` into your current directory.

The command `touch yourgame.c` will simply create an empty file named `yourgame.c`

The command `gcc -O2 -Wall -Werror -g -o yourgame yourgame.c libgamejam.a` is a bit more complicated.
It will compile your code and create an executable binary named `yourgame`.

The option `-O2` tells the compiler that it should try to optimize your code.

The options `-Wall` and `-Werror` tell the compiler that it should display all warnings and that it should convert all warnings to errors.

The option `-g` tells the compiler that it should include debugging information into the binary. (Debuggers like GDB can work with this information.)

The option `-o yourgame` tells the compiler that it should output the binary in a file called `yourgame`.

The rest of the command tells the compiler which files it should use and it consits of your code files (`.c` files) and the library `libgamejam.a`.

### ... when using git and make
If you are using git for your project/game you can add [this project](https://gitlab.gwdg.de/libgamejam/libgamejam) as a [submodule](https://git-scm.com/book/en/v2/Git-Tools-Submodules): `git submodule add https://gitlab.gwdg.de/libgamejam/libgamejam.git`

This will automatically add libgamejam as a submodule and can be found in your project under the directory `libgamejam/`.

Note: Submodules need two extra commands to be executed after cloning your project:
```
git clone <your project url>
cd <your project>
git submodule init
git submodule update
```
This can also be simplified into one command by using just `git clone --recurse-submodules <your project url>`.

#### building
Assuming your project directory structure looks something like this:
```
./
-- libgamejam/
-- -- Makefile
-- -- gamejam.h
-- -- [...]
-- Makefile
-- yourgame.c
-- somecode.c
-- somecode.h
-- .gitmodules
-- [...]
```

Your `Makefile` might look something like this:
```makefile
# This is just an example Makefile.

CFLAGS := -O2 -Wall -Werror -g
CC := gcc

# This will create an executable binary called yourawesomegame.
yourawesomegame: yourgame.o somecode.o libgamejam.a
	$(CC) $^ -o $@

# This will go into the libgamejam/ directory, build the library and create a symlink to libgamejam.a in your directory.
.PHONY: libgamejam.a
libgamejam.a:
	make -C libgamejam lib && \
	ln -f -s libgamejam/libgamejam.a libgamejam.a
```
In every file where you want to use libgamejam library calls you need to include `libgamejam/gamejam.h`.

For example `yourgame.c` might look something like this:
```c
#include "libgamejam/gamejam.h"
#include "somecode.h"

int main(int argc, char** argv) {
	struct termios origtermsettings; // original terminal settings

	if (!draw_init(16, &origtermsettings)) // init draw environment
		return 1;
	draw_term_clear(); // clear terminal

	input_init(); // init input environment

	some_cool_function(); // a function that is in somecode.c

	const int optioncount = 4;
	const char * options[optioncount];
	options[0] = "Hello World";
	options[1] = "This is my awesome game.";
	options[2] = "Currently it just shows this window but someday it will be awesome.";
	options[3] = "Yeah!";
	draw_prompt_selection(-1, "===>>> My awesome game! <<<===", "Press any key to continue.", optioncount, options, ' ', COLOR_WHITE, COLOR_BLUE, COLOR_BRIGHT_BLACK, NULL);
	input_read(INPUT_FLAG_BLOCK, NULL); // wait for key press


	input_destroy(); // destroy input environment
	draw_destroy(&origtermsettings); // destory draw environment
	return 0;
}
```

![yourawesomegame](images/screenshot_example_yourgame.png)

## Documentation

You can always find the latest library specific documentation [here](https://libgamejam.pages.gwdg.de/libgamejam/gamejam_8h.html) and [here as PDF](https://libgamejam.pages.gwdg.de/libgamejam/refman.pdf).

All library specific documentation can also be found in [doc/](doc/) though it may be outdated unless you manually run `make doc`.
A single PDF File containing the documentation can also be found [here](doc/latex/refman.pdf) though it may also be outdated.

The code of the examples is also documented but NOT added to [doc/](doc/)!
Just look in the source code files for the examples. :)

## Sprite Editor

The library comes with a sprite editor which allows you to create simple sprites as used in the library.

You can compile the sprite editor using `make`.
The resulting binary can be executed using `./sprite_editor`.

There are already some example sprites in [sprite_examples/](sprite_examples/).
Most of these examples are based on ASCII-Art from [asciiart.eu](https://www.asciiart.eu).

You can open one of these sprites (here an example cow sprite) by executing the following command:
```
./sprite_editor sprite_examples/cow.sprite open
```

You can also export a sprite as libgamejam compatible C-Code by using __export__:
```
./sprite_editor sprite_examples/cow.sprite export
```
This C-Code is similar to the code used in [example_chess.c](example_chess.c) for setting a sprite.
(You still need to replace `<SID>` with a sprite id.)

You can create a new sprite by using __new__:
```
./sprite_editor <file> new <width> <height>
```

Screenshots:

![sprite editor cat](images/screenshot_sprite_editor_cat.png)

![sprite editor mario](images/screenshot_sprite_editor_mario.png)

![sprite editor help](images/screenshot_sprite_editor_help.png)


## Examples
This library already comes with some example games which use and showcase most features of the library.

### Example: Chess

This is example is supposed to show how to use sprites and how to make a game with blocking input(the game waits for user input).

All the code for this example can be found in `example_chess.c` and `example_chess.h`.
You can compile it using `make`.
The resulting binary can be executed using `./example_chess`.

Screenshots:

![chess start menu](images/screenshot_example_chess_start_menu.png)

![chess possible moves](images/screenshot_example_chess_possible_moves.png)

![chess promotion](images/screenshot_example_chess_promotion.png)

![chess possible moves](images/screenshot_example_chess_checkmate.png)

An error is shown if your terminal is too small.

### Example: Snake

This is example is supposed to show how to use non-blocking input(the game doesn't wait for user input).

All the code for this example can be found in `example_snake.c` and `example_snake.h`.
You can compile it using `make`.
The resulting binary can be executed using `./example_snake`.

Screenshots:

![snake start menu](images/screenshot_example_snake_start_menu.png)

![snake](images/screenshot_example_snake_running.png)

![snake pause](images/screenshot_example_snake_pause.png)

## Nonbuffering Branch

There is a branch called `nonbuffering`.

This branch will drastically increase rendering performance, but it will also (slightly) increase the number of rendering errors.

For most games it shouldn't make a huge difference which branch you use, but if you have a game that rerenders the entire frame multiple times per seconds and needs the added performance, you might want to check it out.

You can select it by running: `git checkout nonbufferig`

You can go back by executing: `git checkout master`

After switching a branch you need to execute `make clean && make` for changes to apply.


## Questions/Bugs/Issues

Please write an Issue in the [Issue-Tracker](https://gitlab.gwdg.de/libgamejam/libgamejam/-/issues), if you have...
- ... any questions or issues. (Please read the documentation before asking questions related to a function in my code.)
- ... found some bug in my code.
- ... any feature requests.

## Legal stuff
This project is licensed under the MIT License. For further information see the [LICENSE](LICENSE) file.

