#include "example_snake.h"
#include "gamejam.h"

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>
#include <ctype.h>
#include <termios.h>

/* ================================ global variables ================================ */
int offset_x_g, offset_y_g; // character offsets to the beginning of the board // needed for correctly drawing everything in the center


/* ================================ main function ================================ */
int main(int argc, char** argv) {
	struct termios origtermsettings; // original terminal settings

	if (!draw_init(16, &origtermsettings)) // init draw environment
		return 1;

	/* Intializes random number generator */
	time_t t;
	srand((unsigned) time(&t));

	struct snake_game game;
	snake_init_game(&game); // init game

	snake_redraw_all(&game); // draw first frame

	input_init();

	snake_start_menu(); // start menu
	snake_redraw_all(&game); // draw everything again

	int running = 1;
	int c;
	int s;
	struct snake_element * tmp;

	int cols, rows;
	int oldcols, oldrows;
	oldcols = draw_term_get_cols();
	oldrows = draw_term_get_rows();

	/* GAME LOOP */
	while (running) {
		c = input_read(INPUT_FLAG_ARROWSTOWASD | INPUT_FLAG_TOLOWER, &s); // read input (non-blocking)
		if (s) {
			switch(c) { // handle input
			case 'w': snake_set_dir(&game,  0, -1); break;
			case 'a': snake_set_dir(&game, -1,  0); break;
			case 's': snake_set_dir(&game,  0,  1); break;
			case 'd': snake_set_dir(&game,  1,  0); break;
			case 'r': // redraw
				snake_redraw_all(&game);
				break;
			case ' ': // toggle wall
				game.solidwall = !(game.solidwall);
				snake_redraw_all(&game);
				break;
			case '?':
			case 'h': // help
				snake_start_menu();
				snake_redraw_all(&game);
				break;
			case 'p': // pause
				snake_pause(&game);
				break;
			case 'q': // quit
				running = 0;
				break;
			case '+': // increase speed
				game.waittime -= 5000l;
				if (game.waittime < 0l)
					game.waittime = 0l;
				snake_redraw_all(&game);
				break;
			case '-': // decrease speed
				game.waittime += 5000l;
				if (game.waittime > 400000l)
					game.waittime = 400000l;
				snake_redraw_all(&game);
				break;
			}
		}
		
		
		snake_draw_point(game.tail->x, game.tail->y, 0); // draw old tail black


		// increase snake length if it just ate an apple
		if (game.justeatenapple) {
			game.justeatenapple = 0;
			tmp = snake_new_element(-1,-1,NULL);
			snake_redraw_all(&game);
		} else {
			tmp = game.tail;
			// convert old prev of tail to new tail
			game.tail->prev->next = NULL;
			game.tail = game.tail->prev;
		}


		// convert tmp to new head
		tmp->x = game.head->x + game.dirx;
		tmp->y = game.head->y + game.diry;

		// handle wall collision
		if (game.solidwall) {
			if (tmp->x >= game.width) running = 0;
			else if (tmp->x < 0) running = 0;
			if (tmp->y >= game.height) running = 0;
			else if (tmp->y < 0) running = 0;
		} else {
			if (tmp->x >= game.width) tmp->x = 0;
			else if (tmp->x < 0) tmp->x = (game.width -1);
			if (tmp->y >= game.height) tmp->y = 0;
			else if (tmp->y < 0) tmp->y = (game.height -1);

		}

		tmp->next = game.head;
		game.head->prev = tmp;
		tmp->prev = NULL;

		game.head = tmp;

		snake_draw_point(game.head->x, game.head->y, 3); // draw new head
		snake_draw_point(game.head->next->x, game.head->next->y, 2); // draw old head

		if (snake_has_eaten_self(&game))
			running = 0;

		if (game.applex == game.head->x && game.appley == game.head->y) {
			// apple eaten
			game.length++;
			game.justeatenapple = 1;
			snake_generate_new_apple(&game);
			snake_redraw_all(&game);
		}

		draw_flush();
	
		// pause game if window resized
		cols = draw_term_get_cols();
		rows = draw_term_get_rows();
		if (cols != oldcols || rows != oldrows)
			snake_pause(&game);
		oldcols = cols;
		oldrows = rows;

		// wait
		usleep(game.waittime);
	}



	/* GAME OVER */

	input_destroy();
	draw_destroy(&origtermsettings); // destory draw environment

	draw_color_reset();

	// print game over to the terminal
	printf("  ===>>> Game Over! <<< === \n");
	printf("  Length: %3d \n", game.length);

	return 0;
}



/* ================================ functions ================================ */

/**
 * Create a new snake element.
 */
struct snake_element * snake_new_element(int x, int y, struct snake_element * prev) {
	struct snake_element * res = malloc(sizeof(struct snake_element));
	res->x = x;
	res->y = y;
	res->prev = prev;
	res->next = NULL;

	return res;
}

/**
 * Is a given point (x,y) somewhere on the snake?
 */
int snake_is_point_on_snake(struct snake_game * game, int x, int y) {
	struct snake_element * cur = game->head;
	while (cur != NULL) {
		if (cur->x == x && cur->y == y)
			return 1;
		cur = cur->next;
	}
	return 0;
}

/**
 * Generate a new apple.
 *
 * @note May halt if there are no free points left on the board.
 */
void snake_generate_new_apple(struct snake_game * game) {
	do {
		game->applex = rand() % game->width;
		game->appley = rand() % game->height;
	} while (snake_is_point_on_snake(game, game->applex,game->appley));
}

/**
 * Init a snake game.
 */
void snake_init_game(struct snake_game * game) {
	game->width = 39; // default board width
	game->height = 22; // default board height
	game->solidwall = 0; // default no solid wall
	game->length = 5; // default length
	game->dirx = 1; // default direction
	game->diry = 0;
	game->waittime = 50000l; // default wait time (50ms)


	game->justeatenapple = 0;

	// generate snake
	game->head = snake_new_element(game->width / 2, game->height / 2, NULL);
	struct snake_element * cur = game->head;
	int i;
	for (i = 0; i < game->length; i++) {
		cur->next = snake_new_element(game->width / 2 + i * (- game->dirx), game->height / 2 + i * (- game->diry), cur);
		cur = cur->next;
	}
	game->tail = cur;

	// generate apple
	snake_generate_new_apple(game);

}

/**
 * Redraw everything.
 */
void snake_redraw_all(struct snake_game * game) {

	int cols, rows;
	cols = draw_term_get_cols();
	rows = draw_term_get_rows();

	draw_color_reset(); // reset colors

	// enforce minimum terminal size
	while (cols < (game->width * 2) + 2 || rows < game->height + 2) {
		printf("Your terminal is too small. Please enlarge it or make your font smaller.\r\n");
		printf("Current size: %dx%d\r\n", cols,rows);
		printf("Minimum needed: %dx%d\r\n", (game->width * 2) + 2,game->height + 2);
		cols = draw_term_get_cols();
		rows = draw_term_get_rows();
		usleep(10000);
		draw_term_clear();
	}


	offset_x_g = (cols/2) - (game->width * 2 / 2) + 1; // center board // update global character drawing offsets
	offset_y_g = (rows/2) - (game->height / 2) + 1;

	draw_term_clear();

	// draw the snake
	snake_draw_point(game->head->x, game->head->y, 3); // head
	struct snake_element * cur = game->head;
	while ((cur = cur->next) != NULL) {
		snake_draw_point(cur->x, cur->y, 2); // element
	}

	// draw the apple
	snake_draw_point(game->applex, game->appley, 1); // apple

	// draw the outline around the board
	static struct draw_border border = {
		'-',
		'|',
		'-',
		'|',
		'+',
		'+',
		'+',
		'+'
	};
	if (game->solidwall) {
		border.n = '#';
		border.e = '#';
		border.s = '#';
		border.w = '#';
	} else {
		border.n = '-';
		border.e = '|';
		border.s = '-';
		border.w = '|';
	}
	if (game->justeatenapple) {
		draw_color_bg(COLOR_RED);
		draw_color_fg(COLOR_WHITE);
	} else {
		draw_color_bg(COLOR_BLUE);
		draw_color_fg(COLOR_WHITE);
	}
	draw_rect_outline(offset_x_g-1,offset_y_g-1,game->width * 2 + 2, game->height + 2, &border);

	// draw title text
	static char buf[256];
	buf[0] = '\0';
	snprintf(buf,255,"===>>> Length: %3d   |   waittime: %4ld ms <<<===", game->length, game->waittime / 1000l);
	draw_print_center(offset_x_g,offset_y_g -1,game->width * 2 , buf, -1);

	draw_flush();
}

/**
 * Has the snake eaten itself?
 */
int snake_has_eaten_self(struct snake_game * game) {
	struct snake_element * cur = game->head;
	while ((cur = cur->next) != NULL) {
		if (cur->x == game->head->x && cur->y == game->head->y)
			return 1;
	}
	return 0;
}

/**
 * Try setting a new direction for the snake.
 * Fails if the new direction is on the same axis as the old one.
 */
void snake_set_dir(struct snake_game * game, int dirx, int diry) {
	if (dirx == game->dirx || diry == game->diry)
		return;
	game->dirx = dirx;
	game->diry = diry;

}

/**
 * Pause the game.
 *
 * @note While the game is paused it is still checking and handling for window resizes.
 */
void snake_pause(struct snake_game * game) {
	const int optioncount = 0;
	const char * options[optioncount];
	

	int cols, rows;
	int oldcols, oldrows;
	oldcols = draw_term_get_cols();
	oldrows = draw_term_get_rows();
	int redraw = 1;
	int s;
	do {
		if (redraw) {
			snake_redraw_all(game);
			draw_prompt_selection(-1, "===>>> Game Paused! <<<===", "Press any key to continue.", optioncount, options, ' ', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);
			redraw = 0;
		} else {
			cols = draw_term_get_cols();
			rows = draw_term_get_rows();
			if (cols != oldcols || rows != oldrows)
				redraw = 1;
			oldcols = cols;
			oldrows = rows;
		}

		input_read(0, &s);
		usleep(1000);


	} while (s == 0); // until key pressed
	snake_redraw_all(game);
}

/**
 * Display the start menu and wait for key press.
 *
 */
void snake_start_menu() {
	const int optioncount = 11;
	const char * options[optioncount];
	options[ 0] = "Welcome to this libgamejam example implementation";
	options[ 1] = "of snake in C!";
	options[ 2] = "";
	options[ 3] = "Controls:";
	options[ 4] = "- WASD or arrow keys : movement";
	options[ 5] = "-              Space : toggle solid wall";
	options[ 6] = "-                'q' : quit";
	options[ 7] = "-                'p' : pause";
	options[ 8] = "-                'r' : redraw everything";
	options[ 9] = "-           '+', '-' : adjust speed";
	options[10] = "-           '?', 'h' : open this help message";
	
	draw_prompt_selection(-1, "===>>> Snake by Jake <<<===", "Press any key to continue.", optioncount, options, ' ', COLOR_WHITE, COLOR_MAGENTA, COLOR_BRIGHT_BLACK, NULL);
	input_read(INPUT_FLAG_BLOCK, NULL); // wait for key press
}
