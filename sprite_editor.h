#ifndef GAMEJAM_SE
#define GAMEJAM_SE

#include <stdio.h>

#define SE_VERSION "v1.0"

#define SE_SID 1
#define SE_SID_COLORS 2

#define SE_FILEHEADER_MAGICNUM "#SPR17E"
#define SE_FILEHEADER_VERSION "001"
#define SE_FILEHEADER_COMMENTLINE "AUTO GENERATED USING sprite_editor! ONLY EDIT THIS FILE BY HAND IF YOU KNOW WHAT YOU ARE DOING!"
#define SE_FILEBODY_SEPERATOR "---"

enum action_e {SE_ACTION_NEW, SE_ACTION_OPEN, SE_ACTION_EXPORT};

struct se_data_s {
	int width;
	int height;
	char * sprite;
	char * fgcolors;
	char * bgcolors;
	char transparent;
	char transparentbgcolora;
	char transparentbgcolorb;
	int selectedwindow; // 0: raw sprite 1: fgcolors 2: bgcolors
	int curx;
	int cury;
	int isblockselection;
	int selx;
	int sely;
	int ispixelmode; // 0: normal mode 1: move and act on two chars at a time
	int isautocursormove; // 0: dont auto move 1: do auto move
	char * spritefile;
	int issaved;
	enum action_e programaction;
};

void se_help_menu();
void se_settings_menu_transparentbgcolora();
void se_settings_menu_transparentbgcolorb();
void se_settings_menu_transparent();
void se_settings_menu();
int se_save_wrapper();
int se_save();
int se_load_expect_char(FILE* stream, char c);
int se_load_expect_line(FILE* stream, const char * str);
int se_load(char * file);
int se_set_sprite_in_lib();
int se_hex_to_col(char c);
int se_is_hex(char c);
int se_init(int width, int height);
void se_free();
void se_draw_raw_sprite();
void se_draw_fgcolors();
void se_draw_bgcolors();

void se_draw_sprite();
void se_draw_cursor(int window, int x, int y);
void se_redraw_all();
void se_print_sprite_as_c();
void se_set_char(char c, int x, int y);
void se_set_block_char(char c);
int se_quit_menu();
void se_replace_chars();
void se_switch_chars();
void printUsage(char * program);
#endif
