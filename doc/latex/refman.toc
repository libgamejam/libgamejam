\@ifundefined {etoctocstyle}{\let \etoc@startlocaltoc \@gobble \let \etoc@settocdepth \@gobble \let \etoc@depthtag \@gobble \let \etoc@setlocaltop \@gobble }{}
\contentsline {chapter}{\numberline {1}Data Structure Index}{1}{chapter.1}%
\contentsline {section}{\numberline {1.1}Data Structures}{1}{section.1.1}%
\contentsline {chapter}{\numberline {2}File Index}{3}{chapter.2}%
\contentsline {section}{\numberline {2.1}File List}{3}{section.2.1}%
\contentsline {chapter}{\numberline {3}Data Structure Documentation}{5}{chapter.3}%
\contentsline {section}{\numberline {3.1}draw\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}border Struct Reference}{5}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Detailed Description}{5}{subsection.3.1.1}%
\contentsline {section}{\numberline {3.2}draw\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}internal Struct Reference}{6}{section.3.2}%
\contentsline {subsection}{\numberline {3.2.1}Detailed Description}{6}{subsection.3.2.1}%
\contentsline {section}{\numberline {3.3}draw\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}internal\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}\_\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}sprite Struct Reference}{6}{section.3.3}%
\contentsline {subsection}{\numberline {3.3.1}Detailed Description}{6}{subsection.3.3.1}%
\contentsline {chapter}{\numberline {4}File Documentation}{7}{chapter.4}%
\contentsline {section}{\numberline {4.1}gamejam.\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}h File Reference}{7}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Detailed Description}{9}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}The draw environment}{9}{subsection.4.1.2}%
\contentsline {subsubsection}{\numberline {4.1.2.1}Weird behaviour}{9}{subsubsection.4.1.2.1}%
\contentsline {subsection}{\numberline {4.1.3}Coordinates}{10}{subsection.4.1.3}%
\contentsline {subsection}{\numberline {4.1.4}Colors\discretionary {\mbox {\scriptsize $\hookleftarrow $}}{}{}:}{10}{subsection.4.1.4}%
\contentsline {subsection}{\numberline {4.1.5}Macro Definition Documentation}{11}{subsection.4.1.5}%
\contentsline {subsubsection}{\numberline {4.1.5.1}draw\_color\_bg}{11}{subsubsection.4.1.5.1}%
\contentsline {subsubsection}{\numberline {4.1.5.2}draw\_color\_fg}{11}{subsubsection.4.1.5.2}%
\contentsline {subsubsection}{\numberline {4.1.5.3}draw\_flush}{11}{subsubsection.4.1.5.3}%
\contentsline {subsection}{\numberline {4.1.6}Function Documentation}{12}{subsection.4.1.6}%
\contentsline {subsubsection}{\numberline {4.1.6.1}draw\_destroy()}{12}{subsubsection.4.1.6.1}%
\contentsline {subsubsection}{\numberline {4.1.6.2}draw\_init()}{12}{subsubsection.4.1.6.2}%
\contentsline {subsubsection}{\numberline {4.1.6.3}draw\_line\_hori()}{13}{subsubsection.4.1.6.3}%
\contentsline {subsubsection}{\numberline {4.1.6.4}draw\_line\_vert()}{13}{subsubsection.4.1.6.4}%
\contentsline {subsubsection}{\numberline {4.1.6.5}draw\_print\_center()}{13}{subsubsection.4.1.6.5}%
\contentsline {subsubsection}{\numberline {4.1.6.6}draw\_print\_fixedwidth()}{14}{subsubsection.4.1.6.6}%
\contentsline {subsubsection}{\numberline {4.1.6.7}draw\_print\_tickertext()}{14}{subsubsection.4.1.6.7}%
\contentsline {subsubsection}{\numberline {4.1.6.8}draw\_prompt\_selection()}{15}{subsubsection.4.1.6.8}%
\contentsline {subsubsection}{\numberline {4.1.6.9}draw\_rect\_fill()}{16}{subsubsection.4.1.6.9}%
\contentsline {subsubsection}{\numberline {4.1.6.10}draw\_rect\_outline()}{16}{subsubsection.4.1.6.10}%
\contentsline {subsubsection}{\numberline {4.1.6.11}draw\_rect\_shadow()}{17}{subsubsection.4.1.6.11}%
\contentsline {subsubsection}{\numberline {4.1.6.12}draw\_shadowed\_box()}{17}{subsubsection.4.1.6.12}%
\contentsline {subsubsection}{\numberline {4.1.6.13}draw\_sprite()}{19}{subsubsection.4.1.6.13}%
\contentsline {subsubsection}{\numberline {4.1.6.14}draw\_sprite\_in\_box()}{19}{subsubsection.4.1.6.14}%
\contentsline {subsubsection}{\numberline {4.1.6.15}draw\_sprite\_set()}{20}{subsubsection.4.1.6.15}%
\contentsline {subsubsection}{\numberline {4.1.6.16}draw\_term\_echo\_hide()}{21}{subsubsection.4.1.6.16}%
\contentsline {subsubsection}{\numberline {4.1.6.17}draw\_term\_echo\_show()}{21}{subsubsection.4.1.6.17}%
\contentsline {subsubsection}{\numberline {4.1.6.18}draw\_term\_get\_cols()}{21}{subsubsection.4.1.6.18}%
\contentsline {subsubsection}{\numberline {4.1.6.19}draw\_term\_get\_rows()}{21}{subsubsection.4.1.6.19}%
\contentsline {subsubsection}{\numberline {4.1.6.20}input\_init()}{22}{subsubsection.4.1.6.20}%
\contentsline {subsubsection}{\numberline {4.1.6.21}input\_read()}{22}{subsubsection.4.1.6.21}%
\contentsline {chapter}{Index}{23}{section*.7}%
